// **************************************************************************
//
// Configuration of User Module
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _MODULE_CFG_H_
#define _MODULE_CFG_H_

// configuration
typedef struct {
  unsigned int          enabled;
  char                  *device;
  unsigned int          baudrate;
  unsigned int          databits;
  char                  *parity;
  unsigned int          stopbits;
  unsigned int          port;
} module_cfg_t;

// load configuration from file
extern void module_cfg_load(module_cfg_t *cfg_ptr);

// save configuration to file
extern int module_cfg_save(module_cfg_t *cfg_ptr);

#endif

