// **************************************************************************
//
// Configuration of User Module
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>

#include "um_cfg.h"

#include "module.h"
#include "module_cfg.h"

// **************************************************************************
// load configuration from file
void module_cfg_load(module_cfg_t *cfg_ptr)
{
  FILE                  *file_ptr;

  file_ptr          = um_cfg_open(MODULE_SETTINGS, "r");
  cfg_ptr->enabled  = um_cfg_get_int(file_ptr, MODULE_PREFIX "ENABLED" );
  cfg_ptr->device   = um_cfg_get_str(file_ptr, MODULE_PREFIX "DEVICE"  );
  cfg_ptr->baudrate = um_cfg_get_int(file_ptr, MODULE_PREFIX "BAUDRATE");
  cfg_ptr->databits = um_cfg_get_int(file_ptr, MODULE_PREFIX "DATABITS");
  cfg_ptr->parity   = um_cfg_get_str(file_ptr, MODULE_PREFIX "PARITY"  );
  cfg_ptr->stopbits = um_cfg_get_int(file_ptr, MODULE_PREFIX "STOPBITS");
  cfg_ptr->port     = um_cfg_get_int(file_ptr, MODULE_PREFIX "PORT"    );
  um_cfg_close(file_ptr);
}

// **************************************************************************
// save configuration to file
int module_cfg_save(module_cfg_t *cfg_ptr)
{
  FILE                  *file_ptr;

  if ((file_ptr = um_cfg_open(MODULE_SETTINGS, "w"))) {
    um_cfg_put_bool(file_ptr, MODULE_PREFIX "ENABLED" , cfg_ptr->enabled    );
    um_cfg_put_str (file_ptr, MODULE_PREFIX "DEVICE"  , cfg_ptr->device     );
    um_cfg_put_int (file_ptr, MODULE_PREFIX "BAUDRATE", cfg_ptr->baudrate, 1);
    um_cfg_put_int (file_ptr, MODULE_PREFIX "DATABITS", cfg_ptr->databits, 1);
    um_cfg_put_str (file_ptr, MODULE_PREFIX "PARITY"  , cfg_ptr->parity     );
    um_cfg_put_int (file_ptr, MODULE_PREFIX "STOPBITS", cfg_ptr->stopbits, 1);
    um_cfg_put_int (file_ptr, MODULE_PREFIX "PORT"    , cfg_ptr->port    , 0);
    um_cfg_save(file_ptr, MODULE_SETTINGS);
    return 1;
  }

  return 0;
}

