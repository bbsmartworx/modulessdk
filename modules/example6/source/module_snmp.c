// **************************************************************************
//
// Net-SNMP library of User Module
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <string.h>

#include "um_macros.h"
#include "um_snmp.h"

#include "module.h"

// **************************************************************************

// registered OID
static oid OID[]            = { 1, 3, 6, 1, 3 };

// the first SUBID
#define SUBID_MIN           1

// the last SUBID
#define SUBID_MAX           2

// **************************************************************************
// MIB handler
static int mib_handler(netsnmp_mib_handler          *mib_ptr,
                       netsnmp_handler_registration *reg_ptr,
                       netsnmp_agent_request_info   *inf_ptr,
                       netsnmp_request_info         *req_ptr)
{
  netsnmp_variable_list *var_ptr;

  UNUSED(mib_ptr);
  UNUSED(reg_ptr);

  if (inf_ptr->mode != MODE_GET) {
    return SNMP_ERR_GENERR;
  }

  while (req_ptr) {
    var_ptr = req_ptr->requestvb;
    switch (var_ptr->name[OID_LENGTH(OID)]) {
      case 1:
        snmp_set_var_typed_value(var_ptr, ASN_OCTET_STR, MODULE_TITLE, strlen(MODULE_TITLE));
        break;
      case 2:
        snmp_set_var_typed_integer(var_ptr, ASN_INTEGER, 0);
        break;
    }
    req_ptr = req_ptr->next;
  }

  return SNMP_ERR_NOERROR;
}

// **************************************************************************
// init function
void init_snmpd(void);
void init_snmpd(void)
{
  netsnmp_register_scalar_group(netsnmp_create_handler_registration(
                                "example", mib_handler, OID, OID_LENGTH(OID),
                                HANDLER_CAN_RONLY), SUBID_MIN, SUBID_MAX);
}

