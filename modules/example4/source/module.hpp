// **************************************************************************
//
// Definitions for User Module
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _MODULE_HPP_
#define _MODULE_HPP_

// module title
#define MODULE_TITLE        "Example 4"

#endif

