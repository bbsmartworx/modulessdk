// **************************************************************************
//
// CGI script of User Module
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#include <string>
#include <sstream>

#include "um_gpio.h"
#include "um_html.h"

#include "module.hpp"

using namespace std;

// **************************************************************************
// convert integer to string
static string int_to_string(int value)
{
  ostringstream         ss;

  ss << value;

  return ss.str();
}

// **************************************************************************
// convert double to string
static string double_to_string(double value)
{
  ostringstream         ss;

  ss << value;

  return ss.str();
}

// **************************************************************************
// main function of CGI script "index.cgi"
int main(void)
{
  string                msg;
  int                   temperature;
  int                   voltage;

  um_html_page_begin(MODULE_TITLE);

  um_html_form_begin(MODULE_TITLE, "System Status", NULL, 0, NULL, NULL);

  um_html_pre_head("Binary Input");

  msg  = "Binary input <b>BIN0</b> is <b>";
  msg += um_gpio_get_bin0() ? "OFF" : "ON";
  msg += "</b>.";

  um_html_pre_text(msg.c_str());

  um_html_pre_head("Binary Output");

  msg  = "Binary output <b>OUT0</b> is <b>";
  msg += um_gpio_get_out0() ? "ON" : "OFF";
  msg += "</b>.";

  um_html_pre_text(msg.c_str());

  um_html_pre_head("Supply Voltage");

  voltage = (um_gpio_get_voltage() + 50) / 100;

  msg  = "Supply voltage is <b>";
  msg += voltage > 0 ? double_to_string(voltage / 10.0) + " V" : "N/A";
  msg += "</b>.";

  um_html_pre_text(msg.c_str());

  um_html_pre_head("Temperature");

  temperature = um_gpio_get_temperature();

  msg  = "Internal temperature is <b>";
  msg += temperature > 0 ? int_to_string(temperature - 273) + " &deg;C" : "N/A";
  msg += "</b>.";

  um_html_pre_text(msg.c_str());

  um_html_form_end(NULL);

  um_html_page_end();

  return 0;
}

