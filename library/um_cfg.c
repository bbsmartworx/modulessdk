// **************************************************************************
//
// Functions for work with configuration
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "um_cfg.h"
#include "um_ip6.h"
#include "um_process.h"

// **************************************************************************
// open configuration file
FILE *um_cfg_open(const char *config_name, const char *open_mode)
{
  return fopen(config_name, open_mode);
}

// **************************************************************************
// close configuration file
void um_cfg_close(FILE *file_ptr)
{
  if (file_ptr) {
    fclose(file_ptr);
  }
}

// **************************************************************************
// close configuration file and generate integrity file
void um_cfg_save(FILE *file_ptr, const char *config_name)
{
  if (file_ptr) {
    fclose(file_ptr);
    // generate integrity file for -S1 platforms only
    if (access("/sbin/hash_generate", F_OK) == 0) {
      um_process_exec("/sbin/hash_generate", config_name, NULL);
    }
  }
}

// **************************************************************************
// verify configuration integrity
int um_cfg_verify(const char *config_path)
{
  if (access("/sbin/hash_verify", F_OK) == 0) {
    return !um_process_exec("/sbin/hash_verify", config_path, NULL);
  }
  return 1;
}

// **************************************************************************
// put string into configuration file
void um_cfg_put_str(FILE *file_ptr, const char *name, const char *value)
{
  if (file_ptr) {
    if (strchr(value, ' ')) {
      fprintf(file_ptr, "%s=\"%.16000s\"\n", name, value);
    } else {
      fprintf(file_ptr, "%s=%.16000s\n", name, value);
    }
  }
}

// **************************************************************************
// put integer into configuration file
void um_cfg_put_int(FILE *file_ptr, const char *name, unsigned int value, int store_zero)
{
  if (file_ptr) {
    if (value || store_zero) {
      fprintf(file_ptr, "%s=%u\n", name, value);
    } else {
      fprintf(file_ptr, "%s=\n", name);
    }
  }
}

// **************************************************************************
// put boolean into configuration file
void um_cfg_put_bool(FILE *file_ptr, const char *name, unsigned int value)
{
  if (file_ptr) {
    fprintf(file_ptr, "%s=%u\n", name, value ? 1 : 0);
  }
}

// **************************************************************************
// put IP address into configuration file
void um_cfg_put_ip(FILE *file_ptr, const char *name, unsigned int value)
{
  if (file_ptr) {
    if (value) {
      fprintf(file_ptr, "%s=%u.%u.%u.%u\n", name,
              (value >> 24) & 0xFF,
              (value >> 16) & 0xFF,
              (value >> 8) & 0xFF,
              (value) & 0xFF);
    } else {
      fprintf(file_ptr, "%s=\n", name);
    }
  }
}

// **************************************************************************
// put IPv6 address into configuration file
void um_cfg_put_ip6(FILE *file_ptr, const char *name, struct in6_addr *value)
{
  char                  ip6_str[INET6_ADDRSTRLEN];

  if (file_ptr) {
    if (value && !um_ip6_addr_equal(value, &in6addr_any) && inet_ntop(AF_INET6, value, ip6_str, sizeof(ip6_str))) {
      fprintf(file_ptr, "%s=%s\n", name, ip6_str);
    } else {
      fprintf(file_ptr, "%s=\n", name);
    }
  }
}

// **************************************************************************
// get value from configuration file
static const char *um_cfg_get(FILE *file_ptr, const char *name)
{
  static char           line[16384];
  char                  *ptr;

  if (file_ptr) {
    rewind(file_ptr);
    while (fgets(line, sizeof(line), file_ptr)) {
      if ((ptr = strchr(line, '\r'))) {
        *ptr = '\0';
      }
      if ((ptr = strchr(line, '\n'))) {
        *ptr = '\0';
      }
      if ((ptr = strrchr(line, '"'))) {
        *ptr = '\0';
      }
      if ((ptr = strchr(line, '='))) {
        *ptr++ = '\0';
        if (!strcasecmp(name, line)) {
          if (*ptr == '"') {
            ptr++;
          }
          return ptr;
        }
      }
    }
  }

  return NULL;
}

// **************************************************************************
// get string from configuration file (result shall be freed by the caller)
char *um_cfg_get_str(FILE *file_ptr, const char *name)
{
  const char            *ptr;

  if ((ptr = um_cfg_get(file_ptr, name))) {
    return strdup(ptr);
  }

  return strdup("");
}

// **************************************************************************
// get integer from configuration file
unsigned int um_cfg_get_int(FILE *file_ptr, const char *name)
{
  unsigned int          value;
  const char            *ptr;

  if ((ptr = um_cfg_get(file_ptr, name))) {
    if (sscanf(ptr, "%u", &value) == 1) {
      return value;
    }
  }

  return 0;
}

// **************************************************************************
// get IP address from configuration file
unsigned int um_cfg_get_ip(FILE *file_ptr, const char *name)
{
  unsigned int          f3, f2, f1, f0;
  const char            *ptr;

  if ((ptr = um_cfg_get(file_ptr, name))) {
    if (sscanf(ptr, "%u.%u.%u.%u", &f3, &f2, &f1, &f0) == 4) {
      return (f3 << 24) + (f2 << 16) + (f1 << 8) + f0;
    }
  }

  return 0;
}

// **************************************************************************
// get IPv6 address from configuration file (result shall be freed by
// the caller)
struct in6_addr *um_cfg_get_ip6(FILE *file_ptr, const char *name)
{
  struct in6_addr       *ip6;
  const char            *ptr;

  if (!(ip6 = malloc(sizeof(struct in6_addr)))) {
    return NULL;
  }

  if ((ptr = um_cfg_get_str(file_ptr, name))) {
    if (inet_pton(AF_INET6, ptr, ip6)) {
      return ip6;
    }
  }

  *ip6 = in6addr_any;

  return ip6;
}