// **************************************************************************
//
// Functions for getting parameters of CGI script
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <pwd.h>
#include <arpa/inet.h>

#include "um_cgi.h"
#include "um_csrf.h"
#include "um_status.h"

// **************************************************************************

// name of uploaded file
#define UPLOAD_FILENAME     "/tmp/upload%d.bin"

// size of table of allowed characters
#define MAX_SAFE_CHAR       128

// IDs of built-in groups
#define ROOT_GROUP_ID       0
#define ADMIN_GROUP_ID      1
#define DAEMONS_GROUP_ID    999
#define USERS_GROUP_ID      1000

// **************************************************************************

// table of allowed characters
static const unsigned char SAFE_CHAR[MAX_SAFE_CHAR] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  1,1,0,1,0,1,0,0,0,0,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,1,1,1,0,0,1,0,1,
  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,
  0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0
};

// **************************************************************************

// parameters of CGI script
static char *query = NULL;

// counter of uploaded files
static int file_index = 1;

// **************************************************************************
// convert character to number
static unsigned int char2int(char ch)
{
  return (ch >= 'A') ? ((ch & 0xDF) - 'A' + 10) : (ch - '0');
}

// **************************************************************************
// add string with conversion of non-standard chars to ESC sequences
static void add_escaped_string(char *dst, int dst_size, char *src, int src_len)
{
  int                   i, j;

  j = strlen(dst);
  for (i = 0; i < src_len && j < dst_size; i++) {
    if (isalnum(*src)) {
      dst[j++] = *src++;
    } else {
      if (j + 4 > dst_size) {
        break;
      }
      j += snprintf(&dst[j], 4, "%%%02X", (int)*src++);
    }
  }
  dst[j] = '\0';
}

// **************************************************************************
// remove all escape sequences from string
static void unescape_string(char *str)
{
  char                  *src, *dst;
  unsigned int          tmp;

  src = dst = str;
  while (*src) {
    if (*src == '%') {
      src++;
      tmp  = char2int(*src++) << 4;
      tmp += char2int(*src++);
    } else {
      tmp  = *src++;
    }
    *dst++ = (char)tmp;
  }
  *dst = '\0';
}

// **************************************************************************
// remove all control characters from string
static void unctrl_string(char *str)
{
  char                  *src, *dst, tmp;

  src = dst = str;
  while (*src) {
    tmp = *src++;
    if (((unsigned char)tmp < MAX_SAFE_CHAR) && SAFE_CHAR[(unsigned char)tmp]) {
      *dst++ = tmp;
    }
  }
  *dst = '\0';
}

// **************************************************************************
// remove all spaces from string
static void unspace_string(char *str)
{
  char                  *src, *dst, tmp;

  src = dst = str;
  while (*src) {
    tmp = *src++;
    if (tmp != ' ') {
      *dst++ = tmp;
    }
  }
  *dst = '\0';
}

// **************************************************************************
// remove all leading whitespace and all carriage returns from string
static void sanitize_string(char *str)
{
  char                  *src, *dst, tmp;

  src = dst = str;
  while (isspace(*src)) {
    src++;
  }
  while (*src) {
    tmp = *src++;
    if (tmp != '\r') {
      *dst++ = tmp;
    }
  }
  *dst = '\0';
}

// **************************************************************************
// get one line from standard input
static int get_line(char *line, int size, int *remain)
{
  int   c, count = 0;

  while (*remain > 0) {
    c = getchar();
    if (c == EOF) {
      break;
    }
    count++;
    (*remain)--;
    *line++ = c;
    if (c == '\n') {
      break;
    }
    if (count >= size - 1) {
      break;
    }
  }

  *line = '\0';

  return count;
}

// **************************************************************************
// parse multipart data
static void um_cgi_parse_multipart(void)
{
  char                  buffer[4096];
  char                  couple[8192];
  char                  upload_file[32];
  char                  *boundary, *name, *temp, *ptr, *filename;
  int                   count_buf, count_act, length;
  FILE                  *fp = NULL;

  // stop, when user is not logged
  if (!um_cgi_get_user_name()[0]) {
    return;
  }

  // get content length
  if ((temp = getenv("CONTENT_LENGTH"))) {
    length = strtoul(temp, NULL, 10);
    // search for beginning of boundary
    if ((temp = getenv("CONTENT_TYPE"))) {
      if ((boundary = strstr(temp, "boundary="))) {
        // get boundary
        boundary += 9;
        // process line
        while ((get_line(buffer, sizeof(buffer), &length))) {
          if (strstr(buffer, "form-data")) {
            if ((name = strstr(buffer, "name=\""))) {
              filename = strstr(buffer, "filename=\"");
              name += 6;
              if ((ptr = strchr(name, '\"'))) {
                *ptr = '\0';
              }
              if (strlen(name)) {
                // create file and/or pair name=value
                if (filename) {
                  filename += 10;
                  if ((ptr = strchr(filename, '\"'))) {
                    *ptr = '\0';
                  }
                  snprintf(upload_file, sizeof(upload_file), UPLOAD_FILENAME, file_index);
                  snprintf(couple, sizeof(couple), "%s%s.file=%s&%s.name=%s", query ? "&" : "", name, upload_file, name, filename);
                  if (!(fp = fopen(upload_file, "w"))) {
                    um_cgi_end();
                    return;
                  }
                } else {
                  snprintf(couple, sizeof(couple), "%s%s=", query ? "&" : "", name);
                }
                // search for empty line
                while (get_line(buffer, sizeof(buffer), &length)) {
                  if ((buffer[0] == '\r') && (buffer[1] == '\n')) {
                    break;
                  }
                }
                // process next part of request until boundary
                count_buf = 0;
                while ((count_act = get_line(&buffer[count_buf], sizeof(buffer) - count_buf, &length))) {
                  if (strstr(buffer, boundary)) {
                    break;
                  }
                  count_buf += count_act;
                  if (count_buf <= 2) {
                    continue;
                  }
                  if (fp) {
                    fwrite(buffer, 1, count_buf - 2, fp);
                  } else {
                    add_escaped_string(couple, sizeof(couple), buffer, count_buf - 2);
                  }
                  memmove(buffer, &buffer[count_buf - 2], 2);
                  count_buf = 2;
                }
                // add pair to current parameters
                if (query) {
                  query = realloc(query, strlen(query) + strlen(couple) + 1);
                  strcat(query, couple);
                } else {
                  query = strdup(couple);
                }
                // close file
                if (fp) {
                  fclose(fp);
                  fp = NULL;
                  file_index++;
                }
              }
            }
          }
        }
      }
    }
  }
}

// **************************************************************************
// start CGI script parameters processing
void um_cgi_begin(void)
{
  char                  *request_method, *content_type, *str;
  size_t                length;

  // return if CGI script parameters have been already processed
  if (query) {
    return;
  }

  // get content type
  content_type = getenv("CONTENT_TYPE");

  // parse multipart data
  if (content_type && strstr(content_type, "multipart/form-data")) {
    um_cgi_parse_multipart();
    return;
  }

  // get request method
  if ((request_method = getenv("REQUEST_METHOD"))) {
    // process parameters passed by POST method
    if (!strcmp(request_method, "POST")) {
      if ((str = getenv("CONTENT_LENGTH"))) {
        length = strtoul(str, NULL, 10);
        query = malloc(length + 1);
        if (fread(query, 1, length, stdin) != length) {
          free(query);
          query = NULL;
          return;
        }
        query[length] = '\0';
      }
    // process parameters passed by GET method
    } else if (!strcmp(request_method, "GET")) {
      if ((str = getenv("QUERY_STRING"))) {
        query = strdup(str);
      }
    }
  }
}

// **************************************************************************
// finish CGI script parameters processing
void um_cgi_end(void)
{
  char                  filename[32];
  int                   i;

  if (query) {
    free(query);
    query = NULL;
  }

  // delete temporary files
  for (i = 1; i < file_index; i++) {
    snprintf(filename, sizeof(filename), UPLOAD_FILENAME, i);
    unlink(filename);
  }
}

// **************************************************************************
// check if query is ok (bez kontroly tiketu CSRF)
int um_cgi_query_ok_not_csrf(void)
{
  return (query != NULL && *query) ? 1 : 0;
}

// **************************************************************************
// check if query is ok (s kontrolou tiketu CSRF)
int um_cgi_query_ok(void)
{
  char   *request_id;

  if (!um_cgi_query_ok_not_csrf()) {
    return 0;
  }

  if (!um_cgi_get_str("request_id", &request_id, 1)) {
    return 0;
  }

  return um_csrf_check_token(request_id);
}

// **************************************************************************
// get CGI script parameter (raw string)
int um_cgi_get_raw(const char *name, char **value_ptr)
{
  char                  *param, *value, *src, *dst;

  if (query) {
    param = calloc(strlen(query) + 1, sizeof(char));
    value = calloc(strlen(query) + 1, sizeof(char));
    src   = query;
    while (*src) {
      dst = param;
      while (*src && (*src != '=')) {
        *dst++ = (char)((*src == '+') ? ' ' : *src);
        src++;
      }
      if (*src) src++;
      *dst = '\0';
      dst = value;
      while (*src && (*src != '&')) {
        *dst++ = (char)((*src == '+') ? ' ' : *src);
        src++;
      }
      if (*src) src++;
      *dst = '\0';
      unescape_string(param);
      if (!strcmp(param, name)) {
        free(param);
        unescape_string(value);
        *value_ptr = value;
        return 1;
      }
    }
    free(param);
    free(value);
  }

  *value_ptr  = malloc(1);
  **value_ptr = '\0';

  return 1;
}

// **************************************************************************
// get CGI script parameter (string)
int um_cgi_get_str(const char *name, char **value_ptr, int no_spaces)
{
  um_cgi_get_raw(name, value_ptr);
  unctrl_string(*value_ptr);
  if (no_spaces) {
    unspace_string(*value_ptr);
  }

  return 1;
}

// **************************************************************************
// get CGI script parameter (multi-line text)
int um_cgi_get_text(const char *name, char **value_ptr)
{
  um_cgi_get_raw(name, value_ptr);
  sanitize_string(*value_ptr);

  return 1;
}

// **************************************************************************
// get CGI script parameter (integer)
int um_cgi_get_int(const char *name, unsigned int *value_ptr)
{
  char                  *str;
  char                  *unconverted;
  unsigned int           candidate;

  um_cgi_get_str(name, &str, 1);

  // NULL or empty string is converted to 0
  if (!str || !*str) {
    free(str);
    *value_ptr = 0;
    return 1;
  }

  candidate = strtoul(str, &unconverted, 10);

  if (*unconverted) {
    free(str);
    *value_ptr = 0;
    return 0;
  }

  free(str);
  *value_ptr = candidate;
  return 1;
}

// **************************************************************************
// get CGI script parameter (boolean)
int um_cgi_get_bool(const char *name, unsigned int *value_ptr)
{
  char                  *str;

  um_cgi_get_raw(name, &str);
  *value_ptr = (strcmp(str, "on") == 0);
  free(str);

  return 1;
}

// **************************************************************************
// get CGI script parameter (IP address)
int um_cgi_get_ip(const char *name, unsigned int *value_ptr, int zero)
{
  char                  *str;
  unsigned int          f0, f1, f2, f3;

  um_cgi_get_str(name, &str, 1);

  if (!*str) {
    *value_ptr = 0;
    free(str);
    return zero;
  }

  if (sscanf(str, "%u.%u.%u.%u", &f3, &f2, &f1, &f0) == 4) {
    if ((f0 | f1 | f2 | f3) <= 255) {
      *value_ptr = (f3 << 24) + (f2 << 16) + (f1 << 8) + f0;
      free(str);
      return *value_ptr || zero;
    }
  }

  free(str);

  return 0;
}

// **************************************************************************
// get CGI script parameter (IPv6 address)
int um_cgi_get_ip6(const char *name, struct in6_addr **value_ptr, int zero)
{
  char                  *str;

  if (!(*value_ptr = malloc(sizeof(struct in6_addr)))) {
    return 0;
  }

  um_cgi_get_str(name, &str, 1);

  if (!*str) {
    **value_ptr = in6addr_any;
    free(str);
    return zero;
  }

  if (inet_pton(AF_INET6, str, *value_ptr) == 1) {
    free(str);
    return 1;
  }

  free(str);

  return 0;
}

// **************************************************************************
// get CGI script parameter (MAC address)
int um_cgi_get_mac(const char *name, char **value_ptr, int zero)
{
  char                  ch;
  unsigned int          f0, f1, f2, f3, f4, f5;

  um_cgi_get_str(name, value_ptr, 1);

  if (!**value_ptr) {
    return zero;
  }

  if (sscanf(*value_ptr, "%x:%x:%x:%x:%x:%x%c", &f5, &f4, &f3, &f2, &f1, &f0, &ch) == 6) {
    if ((f0 | f1 | f2 | f3 | f4 | f5) <= 255) {
      return 1;
    }
  }

  return 0;
}

// **************************************************************************
// get name of logged in user
const char *um_cgi_get_user_name(void)
{
  const char            *username;

  username = getenv("REMOTE_USER");

  return username ? : "";
}

// **************************************************************************
// check role of logged in user
int um_cgi_is_admin_role(void)
{
  const char   *variant;
  gid_t        group;

  variant = um_status_get_os_info("VARIANT");

  group = (strcmp(variant, "S1") == 0) ? ADMIN_GROUP_ID : ROOT_GROUP_ID;

  return (um_cgi_get_user_gid() == group);
}

// **************************************************************************
// get user ID of logged in user
uid_t um_cgi_get_user_uid(void)
{
  struct passwd         *pw_ptr;

  pw_ptr = getpwnam(um_cgi_get_user_name());

  if (pw_ptr) {
    return pw_ptr->pw_uid;
  } else {
    return -1;
  }
}

// **************************************************************************
// get group ID of logged in user
gid_t um_cgi_get_user_gid(void)
{
  struct passwd         *pw_ptr;

  pw_ptr = getpwnam(um_cgi_get_user_name());

  if (pw_ptr) {
    return pw_ptr->pw_gid;
  } else {
    return -1;
  }
}

// **************************************************************************
// get nonce value for JS when unsafe-inline is off
const char* um_cgi_get_nonce_js(void)
{
  char*         nonce;

  if ((nonce = getenv("SAFE_INLINE_JS"))) {
    return nonce;
  } else {
    return "";
  }
}

// **************************************************************************
// get nonce value for CSS when unsafe-inline is off
const char* um_cgi_get_nonce_css(void)
{
  char*         nonce;

  if ((nonce = getenv("SAFE_INLINE_CSS"))) {
    return nonce;
  } else {
    return "";
  }
}
