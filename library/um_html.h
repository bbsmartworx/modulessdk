// **************************************************************************
//
// Functions for generating HTML code
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_HTML_H_
#define _UM_HTML_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <netinet/in.h>
#include <stdbool.h>

#define WEB_COLOR_PRIMARY    "#004280"
#define WEB_COLOR_SECONDARY  "#C0E0FF"
#define WEB_COLOR_ERROR      "#800000"
#define WEB_COLOR_SUCCESS    "#008000"
#define WEB_COLOR_SHADOW     "#B0C0E0"
#define WEB_COLOR_TEXT       "#000000"
#define WEB_COLOR_TITLE      "#FFFFFF"
#define WEB_COLOR_BACKGROUND "#F4F4F4"
#define WEB_COLOR_DISABLED   "#808080"

// macro to run a code (intended for form elements) conditionally for admins only, it inserts info text for non-admins
#define ADMIN_ONLY_HIDE(field_name, c_code) if (um_cgi_is_admin_role()) { \
    c_code; \
  } else { \
    um_html_vtext("<input name=\"%s\" type=\"hidden\" value=\"\"/><div class=\"input-removed\">Visible for admin users only</div>", field_name); \
  }

// macro to run a code (intended for form elements) conditionally for admins only
#define ADMIN_ONLY_REMOVE(c_code) if (um_cgi_is_admin_role()) { \
    c_code; \
  }

// item of select element
typedef struct {
  const char            *text;
  const char            *value;
} um_option_str_t;

// item of select element
typedef struct {
  const char            *text;
  unsigned int          value;
} um_option_int_t;

// item of main menu
typedef struct {
  const char            *label;
  const char            *url;
} um_menu_item_t;

// send HTML code of JavaScript
extern void um_html_js(const char *js, ...);

// send HTML code of external JavaScript
extern void um_html_js_external(const char *filename);

// send HTML code of Cascade Styles
extern void um_html_css(const char *css, ...);

// send HTML code of beginning of page
extern void um_html_page_begin(const char *title);

// send HTML code of beginning admin`s page
extern int um_html_admin_page_begin(const char *title, const char *cgi_name);

// send HTML code of end of page
extern void um_html_page_end(void);

// send HTML code of info box (generic)
extern void um_html_info_box(int ok, const char *title, const char *text, const char *url, const char *button);

// send HTML code of info box (configuration)
extern void um_html_config_info_box(int ok, int input_ok, int reboot, const char *url);

// send HTML code of beginning of form
extern void um_html_form_begin(const char *heading, const char *title, const char *action, int multipart, const char *javascript, const um_menu_item_t *menu);

// send HTML code of end of form
extern void um_html_form_end(const char *javascript);

// send HTML code of horizontal line in form
extern void um_html_form_break(void);

// define structure of table
extern void um_html_table(unsigned int cols, unsigned int align);

// define ID for next cell in table
extern void um_html_id(const char *name);

// define style for next cell in table
extern void um_html_style(const char *style);

// send HTML code of simple text
extern void um_html_text(const char *text);

// send HTML code of variable simple text
void um_html_vtext(const char *format, ...);

// send HTML code of link
extern void um_html_link(const char *url, const char *text);

// send HTML code of input box (string)
extern void um_html_input_str(const char *name, const char *value);

// send HTML code of input box (read only string)
extern void um_html_input_str_ro(const char *name, const char *value);

// send HTML code of input box (hidden string)
extern void um_html_input_str_hd(const char *name, const char *value);

// send HTML code of input box (integer)
extern void um_html_input_int(const char *name, unsigned int value, int show_zero, const char *unit);

// send HTML code of input box (hidden integer)
extern void um_html_input_int_hd(const char *name, unsigned int value);

// send HTML code of input box (IP address)
extern void um_html_input_ip(const char *name, unsigned int value);

// send HTML code of input box (IPv6 address)
extern void um_html_input_ip6(const char *name, const struct in6_addr *value);

// Send HTML code of input password box with optional quality indicator.
// Password is hidden with dots instead text, but user can show it with "eye"
// icon.
// name         - HTML field name
// value        - Curren password
// autocomplete - Enable browser autocomplete functionality
// indicator    - Display password quality indicator
// name_field   - Name of field with user name (or other related field). Can be
//                be NULL, When it's set then password has lower quality if it
//                contains value from this field. I.e. password including user
//                name is weaker.
extern void um_html_input_pwd(const char *name, const char *value, bool autocomplete, bool indicator, const char *name_field);

// send HTML code of input box (file)
extern void um_html_input_file(const char *name);

// send HTML code of select box (string)
extern void um_html_select_str(const char *name, const char *value, const um_option_str_t *option_ptr);

// send HTML code of select box (integer)
extern void um_html_select_int(const char *name, unsigned int value, const um_option_int_t *option_ptr);

// send HTML code of radio button
extern void um_html_radio(const char *name, unsigned int id, unsigned int value);

// send HTML code of check box
extern void um_html_check_box(const char *name, unsigned int checked);

// send HTML code of text area (text)
extern void um_html_area_text(const char *name, const char *value, unsigned int cols, unsigned int rows);

// send HTML code of text area (file)
extern void um_html_area_file(const char *name, const char *filename, unsigned int cols, unsigned int rows);

// send HTML code of submit button
extern void um_html_submit(const char *name, const char *value);

// send HTML code of header of preformatted block
extern void um_html_pre_head(const char *text);

// send HTML code of preformatted block (text)
extern void um_html_pre_text(const char *text);

// send HTML code of preformatted block (file); the entities arg specifies whether to convert special chars to HTML entities
extern void um_html_pre_file(const char *filename, const char *error, bool entities);

// send HTML code of preformatted block (program output); the entities arg specifies whether to convert special chars to HTML entities
extern void um_html_pre_proc(const char *program, bool entities);

// send HTML code of page with system log
extern void um_html_system_log(const char *title, const um_menu_item_t *menu);

// send HTML code of detected invalid hash
extern void um_html_integrity(const char *config_path);

#ifdef __cplusplus
}
#endif

#endif

