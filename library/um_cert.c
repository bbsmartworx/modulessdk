// **************************************************************************
//
// Soubor : um_cert.c
// Autor  : Marek Poslusny
// Popis  : Funkce pro nacteni certifikatu ze souboru
//
// **************************************************************************

#define _GNU_SOURCE

#include <ctype.h>          // isspace
#include <stdio.h>          // fopen, fclose, getline, snprintf
#include <stdlib.h>         // free
#include <string.h>         // str...
#include <unistd.h>         // getpid, unlink

#include "um_cert.h"
#include "um_file.h"        // file_...
#include "um_process.h"     // process_...
#include "um_macros.h"      // UNUSED

// **************************************************************************
// zruseni znaku CR v retezci
static void sanitize_string(char *str)
{
  char                  *src;
  char                  *dst;
  char                  tmp;

  src = dst = str;
  while (*src) {
    tmp = *src++;
    if (tmp != '\r') {
      *dst++ = tmp;
    }
  }
  *dst = '\0';
}

// **************************************************************************
// nacteni certifikatu ve formatu PEM
static int cert_load_pem(const char *filename, const char *type, char *buffer, size_t buffer_size)
{
  FILE                  *file_ptr;
  char                  begin_mark[64];
  char                  end_mark[64];
  char                  *line_ptr;
  size_t                line_len;
  int                   found;
  int                   result;

  // inicializace promenych
  line_ptr = NULL;
  line_len = 0;
  found    = 0;
  result   = 0;

  // priprava vyhledavanych oddelovacu
  snprintf(begin_mark, sizeof(begin_mark), "-----BEGIN %s-----\n", type);
  snprintf(end_mark  , sizeof(end_mark)  , "-----END %s-----\n"  , type);

  // konec, pokud se nepodarilo otevrit soubor
  if (!(file_ptr = fopen(filename, "r"))) {
    return 0;
  }

  // vyhledani a nacteni certifikatu
  while (getline(&line_ptr, &line_len, file_ptr) > 0) {
    sanitize_string(line_ptr);
    if (!found) {
      found = !strcmp(line_ptr, begin_mark);
    }
    if (!found) {
      continue;
    }
    strncat(buffer, line_ptr, buffer_size - strlen(buffer) - 1);
    if (!strcmp(line_ptr, end_mark)) {
      result = strlen(buffer) < buffer_size - 1;
      break;
    }
  }

  // uvolneni pameti
  if (line_ptr != NULL) {
    free(line_ptr);
  }

  // uzavreni souboru
  fclose(file_ptr);

  // reinicializace obsahu bufferu v pripade chyby
  if (!result) {
    buffer[0] = '\0';
  }

  // vraceni vysledku
  return result;
}

// **************************************************************************
// nacteni certifikatu ve formatu PKCS#12
static int cert_load_pkcs12(const char *filename, const char *passwd, const char *type, char *buffer, size_t buffer_size)
{
  char                  tmp_file[32];
  char                  pass_str[128];
  int                   result;

  // priprava parametru
  snprintf(tmp_file, sizeof(tmp_file), "/tmp/tmp_%05X.pem", getpid());
  snprintf(pass_str, sizeof(pass_str), "pass:%s", passwd ? : "");

  // konverze certifikatu ve formatu PKCS#12 do formatu PEM
  if (um_process_exec("/usr/bin/openssl",
                      "pkcs12",
                      "-in", filename,
                      "-out", tmp_file,
                      "-passin", pass_str,
                      strcmp(type, "CA CERTIFICATE") ? "-clcerts" : "-cacerts",
                      "-nodes",
                      "-nomacver")) {
    unlink(tmp_file);
    return 0;
  }

  // nacteni certifikatu ve formatu PEM
  result = cert_load_pem(tmp_file, strstr(type, "CERTIFICATE") ? : type, buffer, buffer_size);

  // smazani docasneho souboru
  unlink(tmp_file);

  // vraceni vysledku
  return result;
}

// **************************************************************************
// nacteni certifikatu ze souboru
int um_cert_load(const char *filename, const char *passwd, const char *type, char *buffer, size_t buffer_size)
{
  int                   result;

  UNUSED(passwd);

  // konec, pokud neni vystupni buffer pouzitelny
  if (buffer == NULL || buffer_size < 2048) {
    return 0;
  }

  // inicializace obsahu bufferu
  buffer[0] = '\0';

  // konec, pokud neni jmeno souboru platne
  if (!filename[0]) {
    return 1;
  }

  // konec, pokud je soubor prazdny
  if (!um_file_size(filename)) {
    return 1;
  }

  // nacteni certifikatu ve formatu PEM
  if (strstr(type, "CERTIFICATE")) {
    result = cert_load_pem(filename, "CERTIFICATE", buffer, buffer_size);
  } else if (strstr(type, "KEY")) {
    result = cert_load_pem(filename, "PRIVATE KEY"          , buffer, buffer_size) ||
             cert_load_pem(filename, "ENCRYPTED PRIVATE KEY", buffer, buffer_size) ||
             cert_load_pem(filename, "RSA PRIVATE KEY"      , buffer, buffer_size);
  } else {
    result = cert_load_pem(filename, type, buffer, buffer_size);
  }

  // nacteni certifikatu ve formatu PKCS#12, pokud predchozi krok selhal
  if (!result) {
    result = cert_load_pkcs12(filename, passwd, type, buffer, buffer_size);
  }

  // vraceni vysledku
  return result;
}
