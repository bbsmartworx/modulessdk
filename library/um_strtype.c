// **************************************************************************
//
// String manipulation library
//
// Copyright (C) 2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>

#include "um_strtype.h"

// Converts hexadecimal value from char to integer
#define HEX_CTOI(hex_char) ((hex_char & 0xF) + (hex_char >> 6) * 9)

// Decodes C string from an unprefixed, unseparated hexadecimally encoded C string.
// NULL is returned in case of memory allocation error.
char *um_hex2str(const char *hex_str)
{
  unsigned buf_size;
  char *str, *out_ptr;
  char hex_upper_half;
  
  // Allocate one character also for last half octet (if present).
  buf_size = (unsigned)sizeof(char) * (strlen(hex_str) + 1) / 2 + 1;
  str = malloc(buf_size);
  if (str == NULL) {
    return NULL;
  }
  out_ptr = str;

  while (out_ptr < str + buf_size - 1) {
    hex_upper_half = *hex_str++;
    *out_ptr++ = (HEX_CTOI(hex_upper_half) << 4) + HEX_CTOI(*hex_str);
    hex_str++;
  }
  *out_ptr = '\0';
  return str;
}

// Encodes C string into an unprefixed, unseparated hexadecimally encoded C string.
// NULL is returned in case of memory allocation error.
char *um_str2hex(const char *str)
{
  static const char LUT[] = "0123456789ABCDEF";
  unsigned buf_size;
  char *hex_str, *out_ptr;

  buf_size = (unsigned)sizeof(char) * strlen(str) * 2 + 1;
  hex_str = malloc(buf_size);
  if (hex_str == NULL) {
    return NULL;
  }
  out_ptr = hex_str;

  while (out_ptr < hex_str + buf_size - 2) {
    *out_ptr++ = LUT[*str >> 4];
    *out_ptr++ = LUT[*str & 0xF];
    str++;
  }
  *out_ptr = '\0';
  return hex_str;
}

// Removes period (.) and colon (:) separators from a valid C-string-encoded
// hexadecimal value. Empty hexadecimal groups of length other than 2 are
// not supported.
// NULL is returned in case of memory allocation error.
char *um_hex_remove_separators(const char *in_str)
{
  unsigned buf_size;
  const char *in_ptr;
  char *out_str, *out_ptr;

  in_ptr = in_str;
  buf_size = (unsigned)sizeof(char) * strlen(in_str) + 1;
  out_str = malloc(buf_size);
  if (out_str == NULL) {
    return NULL;
  }
  out_ptr = out_str;

  while (in_ptr < in_str + buf_size - 1) {
    if (*in_ptr == '.' || *in_ptr == ':') {
      in_ptr++;
    }
    *out_ptr++ = *in_ptr++;
  }
  *out_ptr = '\0';
  return out_str;
}

// Replaces all occurrences of the character in the string.
void um_char_replace(char *str, char old_char, char new_char)
{
  while (*str) {
    if (*str == old_char) {
      *str = new_char;
    }
    str++;
  }
}

// Replaces all occurrences of the substring in the string.
// Caller must provide memory block of sufficient size for all replacements, otherwise
// the string is truncated.
void um_substr_replace(char *str, char *old_substr, char *new_substr, size_t bufsize)
{
  char     *s;
  size_t   old_len, new_len;

  s = str;
  old_len = strlen(old_substr);
  new_len = strlen(new_substr);

  while (s && *s) {
    s = strstr(s, old_substr);
    if (s) {
      if (new_len > old_len) {
        memmove(s + new_len, s + old_len, bufsize - (s - str) - new_len);
      } else if (new_len < old_len) {
        memmove(s + new_len, s + old_len, bufsize - (s - str) - old_len);
      }
      memcpy(s, new_substr, new_len);
      s += new_len;
    }
  }
}

// Counts occurrences of the substring in the string.
unsigned int um_substr_occurrences(char *haystack, char *needle)
{
  unsigned int   count;
  size_t         len;

  count = 0;
  len = strlen(needle);

  while ((haystack = strstr(haystack, needle))) {
    count++;
    haystack += len;
  }

  return count;
}
