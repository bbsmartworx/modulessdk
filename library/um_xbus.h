// **************************************************************************
//
// Functions for communication via interprocess bus
//
// Copyright (C) 2020-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_XBUS_H_
#define _UM_XBUS_H_

#ifdef __cplusplus
extern "C" {
#endif

// check message broker availability
extern int um_xbus_available(void);

// connect to message broker
extern void um_xbus_connect(void);

// disconnect from message broker
extern void um_xbus_disconnect(void);

// subscribe to particular topic
extern void um_xbus_subscribe(const char *topic);

// unsubscribe from particular topic
extern void um_xbus_unsubscribe(const char *topic);

// publish message
extern void um_xbus_publish(const char *topic, const char *payload);

// publish and store message
extern void um_xbus_write(const char *topic, const char *payload);

// read stored message
extern char *um_xbus_read(const char *topic);

// get list of stored messages
extern char *um_xbus_list(void);

// receive message
extern char *um_xbus_receive(char **topic);

// check pending unread messages
extern int um_xbus_pending(void);

// get socket descriptor
extern int um_xbus_socket(void);

#ifdef __cplusplus
}
#endif

#endif

