// **************************************************************************
//
// Functions for work with IPv6
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include "um_ip6.h"

// comparison of two IPv6 addresses
int um_ip6_addr_equal(const struct in6_addr *ip6_1, const struct in6_addr *ip6_2)
{
  if (!ip6_1 || !ip6_2) {
    return 0;
  }

  return (ip6_1->s6_addr32[0] == ip6_2->s6_addr32[0]) &&
         (ip6_1->s6_addr32[1] == ip6_2->s6_addr32[1]) &&
         (ip6_1->s6_addr32[2] == ip6_2->s6_addr32[2]) &&
         (ip6_1->s6_addr32[3] == ip6_2->s6_addr32[3]);
}