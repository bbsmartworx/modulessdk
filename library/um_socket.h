// **************************************************************************
//
// Functions for work with sockets
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_SOCKET_H_
#define _UM_SOCKET_H_

#ifdef __cplusplus
extern "C" {
#endif

// open TCP socket (server mode)
extern int um_socket_open_tcp_server(unsigned int port);

// open TCP socket (client mode)
extern int um_socket_open_tcp_client(unsigned int ip, unsigned int port);

// open UDP socket (server mode)
extern int um_socket_open_udp_server(unsigned int port);

// open UDP socket (client mode)
extern int um_socket_open_udp_client(unsigned int ip, unsigned int port);

// open UNIX socket (server mode)
extern int um_socket_open_unix_server(const char *path);

// open UNIX socket (client mode)
extern int um_socket_open_unix_client(const char *path);

// close socket
extern void um_socket_close(int sock);

// enable keepalive on TCP socket
extern void um_socket_keepalive(int sock, int time, int intvl, int probes);

#ifdef __cplusplus
}
#endif

#endif

