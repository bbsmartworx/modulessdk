// **************************************************************************
//
// Soubor : um_csrf.c
// Autor  : Jan Mazura, Marek Černocký
// Popis  : CSRF attack protection
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>          // snprintf
#include <string.h>         // strlen
#include <ctype.h>          // isdigit
#include <fcntl.h>          // creat
#include <unistd.h>         // close, unlink
#include <sys/time.h>       // gettimeofday
#include <errno.h>          // errno

#include "um_csrf.h"
#include "um_file.h"

#define NEW_RUNTIME_PATH "/run/httpd/"
#define OLD_RUNTIME_PATH "/var/httpd/"

// **************************************************************************
// generate token
char *um_csrf_gen_token(void)
{
  struct timeval        tv;
  static char           token[32];
  char                  filename[64];
  int                   fd;

  // get the current time
  gettimeofday(&tv, NULL);

  // create a token
  snprintf(token, sizeof(token), "%06lu", tv.tv_usec % 1000000UL);

  // create filename
  snprintf(filename, sizeof(filename), NEW_RUNTIME_PATH "%s", token);
  fd = creat(filename, S_IRUSR | S_IWUSR);

  // if runtime httpd folder doesn't exist, try httpd runtime folder of older FW
  if (errno == ENOENT) {
    snprintf(filename, sizeof(filename), OLD_RUNTIME_PATH "%s", token);
    fd = creat(filename, S_IRUSR | S_IWUSR);
  }

  // check if created
  if (fd >= 0) {
    close(fd);
    return token;
  } else {
    return NULL;
  }
}

// **************************************************************************
// check token
int um_csrf_check_token(const char *token)
{
  char                  filename[64];
  const char            *ptr;

  // finish when a token isn't specified
  if (!token) {
    return 0;
  }

  // finish when the token lenght is wrong
  if (strlen(token) != 6) {
    return 0;
  }

  // finish when the token contains invalid characters
  for (ptr = token; *ptr; ptr++) {
    if (!isdigit(*ptr)) {
      return 0;
    }
  }

  // create a filename
  // older FW has different httpd runtime folder
  if (um_file_exists(NEW_RUNTIME_PATH)) {
    snprintf(filename, sizeof(filename), NEW_RUNTIME_PATH "%s", token);
  } else {
    snprintf(filename, sizeof(filename), OLD_RUNTIME_PATH "%s", token);
  }

  // remove file
  return !unlink(filename);
}

