// **************************************************************************
//
// Functions for getting information on network interfaces
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>          // fopen, fclose, fgets, fscanf, sscanf, snprintf
#include <string.h>         // str..., mem...
#include <errno.h>          // errno
#include <unistd.h>         // close
#include <syslog.h>         // syslog
#include <sys/ioctl.h>      // ioctl
#include <sys/socket.h>     // socket
#include <arpa/inet.h>      // htonl, ntohl
#include <net/if.h>         // ifreq

#include "um_iface.h"
#include "um_file.h"

#define UNCHECKED(x) if (x) {}

#define HWADDR_TO_UINT64(hwaddr) \
  ( ((uint64_t)ntohl(*(uint32_t*)hwaddr) << 16) + ntohs(*(uint16_t*)&hwaddr[4]) )

// **************************************************************************
// correct interface MAC address when VRRP is enabled
void um_iface_fix_hwaddr(const char *name, unsigned char *hwaddr)
{
  char                  filename[32];
  unsigned int          tmp[6];
  int                   i;

  if (hwaddr[0] != 0x00 || hwaddr[1] != 0x00 || hwaddr[2] != 0x5E) {
    return;
  }

  snprintf(filename, sizeof(filename), "/run/vrrp/%s.mac", name);
  if (sscanf(um_file_gets(filename), "%02X:%02X:%02X:%02X:%02X:%02X",
             &tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5]) == 6) {
    for (i = 0; i < 6; i++) {
      hwaddr[i] = tmp[i];
    }
  }
}

// **********************************************************************************************
// get interface properties
void um_iface_get_info(const char *name, iface_info_t *info_ptr)
{
  struct ifreq          ifr;
  int                   sock;
  struct in6_addr       ip6;
  FILE                  *file_ptr;
  char                  line[512];
  int                   tmp[16];
  int                   index;
  int                   netmask6;
  int                   scope6;
  int                   count;
  int                   i;

  // initialize structure
  memset(info_ptr, 0, sizeof(*info_ptr));

  // check if interface name was given
  if (!name) {
    return;
  }

  // open socket
  if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
    syslog(LOG_ERR, "socket error: %s", strerror(errno));
    return;
  }

  // get interface index
  snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", name);
  if (ioctl(sock, SIOCGIFINDEX, &ifr) == 0) {
    info_ptr->index = ifr.ifr_ifindex;
  }

  // get interface flags
  snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", name);
  if (ioctl(sock, SIOCGIFFLAGS, &ifr) == 0) {
    info_ptr->flags = ifr.ifr_flags;
    info_ptr->up    = ifr.ifr_flags & IFF_UP;
    info_ptr->run   = ifr.ifr_flags & IFF_RUNNING;
  }

  // get interface hardware address
  snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", name);
  if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0) {
    memcpy(info_ptr->hwaddr, ifr.ifr_hwaddr.sa_data, sizeof(info_ptr->hwaddr));
    um_iface_fix_hwaddr(name, info_ptr->hwaddr);
  }

  // get interface IP address
  snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", name);
  ifr.ifr_addr.sa_family = AF_INET;
  if (ioctl(sock, SIOCGIFADDR, &ifr) == 0) {
    info_ptr->ipaddr = ntohl(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr.s_addr);
  }

  // get interface network mask
  snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", name);
  if (ioctl(sock, SIOCGIFNETMASK, &ifr) == 0) {
    info_ptr->netmask = ntohl(((struct sockaddr_in *)&ifr.ifr_netmask)->sin_addr.s_addr);
  }

  // get interface MTU
  snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", name);
  if (ioctl(sock, SIOCGIFMTU, &ifr) == 0) {
    info_ptr->mtu = ifr.ifr_mtu;
  }

  // get interface IPv6 address
  if ((file_ptr = fopen("/proc/net/if_inet6", "r"))) {
    count = 0;
    while (fgets(line, sizeof(line), file_ptr)) {
      if (sscanf(line, "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x %08x %02x %02x",
                 &tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5], &tmp[6], &tmp[7],
                 &tmp[8], &tmp[9], &tmp[10], &tmp[11], &tmp[12], &tmp[13], &tmp[14], &tmp[15],
                 &index, &netmask6, &scope6) == 19) {
        if (index == info_ptr->index) {
          for (i = 0; i < 16; i++) {
            ip6.s6_addr[i] = tmp[i];
          }
          if (count < IFACE_IPV6_MAX) {
            info_ptr->ipaddr6[count]  = ip6;
            info_ptr->netmask6[count] = netmask6;
            info_ptr->scope6[count]   = scope6 & 0xF0;
            count++;
          }
        }
      }
    }
    fclose(file_ptr);
  }

  // close socket
  close(sock);
}

// **********************************************************************************************
// get interface hardware address
uint64_t um_iface_get_hwaddr(const char *name) {
  iface_info_t          info;

  um_iface_get_info(name, &info);

  return HWADDR_TO_UINT64(info.hwaddr);
}

// **********************************************************************************************
// get interface IP address
unsigned int um_iface_get_ip(const char *name)
{
  iface_info_t          info;

  // zjisteni parametru rozhrani
  um_iface_get_info(name, &info);

  // vraceni IP adresy rozhrani
  return info.up ? info.ipaddr : 0;
}

// **************************************************************************
// get name of the interface to be used to send data to a destination address
const char *um_iface_get_name(unsigned int ip)
{
  FILE                  *file_ptr;
  char                  name[64];
  static char           best_name[64];
  int                   flags, ref, use, metric, mtu, win, ir;
  unsigned long int     dest, gw, mask, best_mask;

  // prepare interface name and network mask
  best_name[0] = '\0';
  best_mask    = 0;

  // open route table
  if ((file_ptr = fopen("/proc/net/route", "r")) == NULL) {
    return NULL;
  }

  // skip first line
  if (fscanf(file_ptr, "%*[^\n]\n") < 0) {
    fclose(file_ptr);
    return NULL;
  }

  // search route table
  while (1) {
    // read one line of the route table
    if (fscanf(file_ptr, "%63s%lx%lx%X%d%d%d%lx%d%d%d\n",
               name, &dest, &gw, &flags, &ref, &use,
               &metric, &mask, &mtu, &win, &ir) != 11) {
      break;
    }

    // skip inactive interfaces
    if (!(flags & 0x0001)) {
      continue;
    }

    // save interface name and network mask if a suitable route was found
    if (((dest & mask) == (htonl(ip) & mask)) && (mask >= best_mask)) {
      strcpy(best_name, name);
      best_mask = mask;
    }
  }

  // close route table
  fclose(file_ptr);

  // return interface name or NULL if no interface was found
  return best_name[0] ? best_name : NULL;
}