// **************************************************************************
//
// Soubor : um_cert.h
// Autor  : Marek Poslusny
// Popis  : Funkce pro nacteni certifikatu ze souboru
//
// **************************************************************************

#ifndef _UM_CERT_H_
#define _UM_CERT_H_

// podporovane formaty certifikatu
#define CERT_CRT_EXT    ".pem, .crt, .p12"
#define CERT_KEY_EXT    ".pem, .key, .p12"
#define CERT_DH_EXT     ".pem"

// nacteni certifikatu ze souboru
extern int um_cert_load(const char *filename, const char *passwd, const char *type, char *buffer, size_t buffer_size);

#endif

