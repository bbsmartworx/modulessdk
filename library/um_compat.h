// **************************************************************************
//
// The GNU C library compatibility layer
//
// Copyright (C) 2019-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_COMPAT_H_
#define _UM_COMPAT_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __arm__

// use the old C++ ABI
#define _GLIBCXX_USE_CXX11_ABI 0

// use older symbol versions
#ifdef __ASSEMBLER__
.symver exp,exp@GLIBC_2.4
.symver expf,expf@GLIBC_2.4
.symver exp2,exp2@GLIBC_2.4
.symver exp2f,exp2f@GLIBC_2.4
.symver fcntl,fcntl@GLIBC_2.4
.symver fcntl64,fcntl@GLIBC_2.4
.symver glob,glob@GLIBC_2.4
.symver glob64,glob64@GLIBC_2.4
.symver log,log@GLIBC_2.4
.symver logf,logf@GLIBC_2.4
.symver log2,log2@GLIBC_2.4
.symver log2f,log2f@GLIBC_2.4
.symver pow,pow@GLIBC_2.4
.symver powf,powf@GLIBC_2.4
#else
__asm__(".symver exp,exp@GLIBC_2.4");
__asm__(".symver expf,expf@GLIBC_2.4");
__asm__(".symver exp2,exp2@GLIBC_2.4");
__asm__(".symver exp2f,exp2f@GLIBC_2.4");
__asm__(".symver fcntl,fcntl@GLIBC_2.4");
__asm__(".symver fcntl64,fcntl@GLIBC_2.4");
__asm__(".symver glob,glob@GLIBC_2.4");
__asm__(".symver glob64,glob64@GLIBC_2.4");
__asm__(".symver log,log@GLIBC_2.4");
__asm__(".symver logf,logf@GLIBC_2.4");
__asm__(".symver log2,log2@GLIBC_2.4");
__asm__(".symver log2f,log2f@GLIBC_2.4");
__asm__(".symver pow,pow@GLIBC_2.4");
__asm__(".symver powf,powf@GLIBC_2.4");
#endif

#endif

#ifdef __cplusplus
}
#endif

#endif

