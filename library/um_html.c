// **************************************************************************
//
// Functions for generating HTML code
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "um_cfg.h"
#include "um_cgi.h"
#include "um_csrf.h"
#include "um_html.h"
#include "um_http.h"
#include "um_ip6.h"
#include "um_macros.h"
#include "um_process.h"

#define HTML_FILTER " | sed 's/\\&/\\&amp;/g;s/</\\&lt;/g;s/>/\\&gt;/g'"

// Page begin scripts

#define JS_SessionTimeout \
  "try {\n" \
  "  function sessionMessage(event) {\n" \
  "    if (event.data == '0') {\n" \
  "      sessionLogout();\n" \
  "    } else {\n" \
  "      clearTimeout(sessionTimeout);\n" \
  "      sessionTimeout = setTimeout(sessionLogout, event.data*1000);\n" \
  "    }\n" \
  "  }\n" \
  "  function sessionLogout() {\n" \
  "    sessionChannel.removeEventListener('message', sessionMessage);\n" \
  "    clearTimeout(sessionTimeout);\n" \
  "    window.location.href = '/login.cgi';\n" \
  "  }\n" \
  "  var sessionChannel = new BroadcastChannel('session-timeout');\n" \
  "  var sessionTimeout = setTimeout(sessionLogout, %s000);\n" \
  "  sessionChannel.postMessage('%s');\n" \
  "  sessionChannel.addEventListener('message', sessionMessage);\n" \
  "} catch {\n" \
  "  // ignore\n" \
  "}\n"

#define JS_GetValue \
   "function GetValue(obj) {\n" \
   "  return obj.value.replace(/\\s/g, '');\n" \
   "}\n"

#define JS_Error \
  "function Error(msg, obj) {\n" \
  "  alert(msg); obj.focus(); obj.select(); return false;\n" \
   "}\n"

#define JS_IsEmpty \
  "function IsEmpty(obj) {\n" \
  "  return GetValue(obj).length == 0;\n" \
  "}\n"

#define JS_IsInRange \
  "function IsInRange(obj, low, high, empty) {\n" \
  "  var str = GetValue(obj);\n" \
  "  unsigned_int_or_empty = /^\\d*$/;\n" \
  "  if (!unsigned_int_or_empty.test(str)) return false;\n" \
  "  if (empty && str.length == 0) return true;\n" \
  "  return (parseInt(str) >= low) && (parseInt(str) <= high);\n" \
  "}\n"

#define JS_IsValidString \
  "function IsValidString(obj, empty) {\n" \
  "  var str = GetValue(obj);\n" \
  "  if (empty && str.length == 0) return true;\n" \
  "  return str.match(/^[ a-zA-Z0-9\\!\\#\\%%\\*\\+\\,\\-\\.\\/\\:\\=\\?\\@\\[\\]\\_\\{\\}\\~]+$/);\n" \
  "}\n"

#define JS_IsValidMAC \
  "function IsValidMAC(obj, empty) {\n" \
  "  var str = GetValue(obj);\n" \
  "  if (empty && str.length == 0) return true;\n" \
  "  return str.search(/^[0-9a-fA-F]{1,2}(:[0-9a-fA-F]{1,2}){5}$/) >= 0;\n" \
  "}\n"

#define JS_IsValidIP \
  "function IsValidIP(obj, empty) {\n" \
  "  var str = GetValue(obj);\n" \
  "  if (empty && str.length == 0) return true;\n" \
  "  var fields = str.match(/^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$/);\n" \
  "  if (fields != null) {\n" \
  "    var tmp = fields[1] | fields[2] | fields[3] | fields[4];\n" \
  "    return (tmp < 256) && (empty || tmp > 0);\n" \
  "  } else {\n" \
  "    return false;\n" \
  "  }\n" \
  "}\n"

#define JS_IsValidIP6 \
  "function IsValidIP6(obj, empty) {\n" \
  "  var i, j;\n" \
  "  var str = GetValue(obj).toLowerCase();\n" \
  "  if (empty && str.length == 0) return true;\n" \
  "  if (!((str.split(':').length == 8) || ((str.split(':').length <= 8) && (str.split('::').length == 2)))) return false;\n" \
  "  var fields = str.match(/^((?:[\\da-f]{1,4}(?::|)){0,8})(::)?((?:[\\da-f]{1,4}(?::|)){0,8})$/);\n" \
  "  if (!fields) return false;\n" \
  "  for (j = 1; j < 4; j++) {\n" \
  "    if (j === 2 || fields[j].length === 0) {\n" \
  "      continue;\n" \
  "    }\n" \
  "    fields[j] = fields[j].split(':');\n" \
  "    for (i = 0; i < fields[j].length; i++) {\n" \
  "      fields[j][i] = parseInt(fields[j][i], 16);\n" \
  "      if (isNaN(fields[j][i]) || (fields[j][i] > 0xFFFF)) {\n" \
  "        return false;\n" \
  "      }\n" \
  "    }\n" \
  "  }\n" \
  "  return true;\n" \
  "}\n"

#define JS_IsValidPrefix \
  "function IsValidPrefix(obj, empty) {\n" \
  "  var str = GetValue(obj);\n" \
  "  if (empty && str.length == 0) return true;\n" \
  "  var value = parseInt(str);\n" \
  "  return (value > 0) && (value <= 128);\n" \
  "}\n"

#define JS_IsValidPort \
  "function IsValidPort(obj, empty) {\n" \
  "  var str = GetValue(obj);\n" \
  "  unsigned_int_or_empty = /^\\d*$/;\n" \
  "  if (!unsigned_int_or_empty.test(str)) return false;\n" \
  "  if (empty && str.length == 0) return true;\n" \
  "  var value = parseInt(str);\n" \
  "  return (value > 0) && (value < 65536);\n" \
  "}\n"

#define JS_ShowOption \
  "function ShowOption(formElement) {\n" \
  "  if (formElement) {\n" \
  "    formElement.closest('tr').style.display = 'table-row';\n" \
  "  }\n" \
  "}\n"

#define JS_HideOption \
  "function HideOption(formElement) {\n" \
  "  if (formElement) {\n" \
  "    formElement.closest('tr').style.display = 'none';\n" \
  "  }\n" \
  "}\n"

// Return how many clases of characters the string contains. The character classes are
// lower case, upper case, digits and other. See "man pam_pwquality"
#define JS_GetClass \
  "function GetClass(s) {\n" \
  "  let lower = 0, upper = 0, digit = 0, other = 0;\n" \
  "  let reLower = /\\p{Ll}/u, reUpper = /\\p{Lu}/u, reDigit = /\\p{Nd}/u;\n" \
  "  for (const c of s) {\n" \
  "    if (reLower.test(c)) lower = 1;\n" \
  "    else if (reUpper.test(c)) upper = 1;\n" \
  "    else if (reDigit.test(c)) digit = 1;\n" \
  "    else other = 1;\n" \
  "  }\n" \
  "  return lower + upper + digit + other;\n" \
  "}\n"

// Return the maximum number of sequential repeats of any character in the string.
#define JS_GetRepeat \
  "function GetRepeat(s) {\n" \
  "  let repeat = 0;\n" \
  "  let r, j;\n" \
  "  [...s].forEach((c, i) => {\n" \
  "    r = 1;\n" \
  "    j = i;\n" \
  "    while (++j < s.length && c == s[j])\n" \
  "      r++;\n" \
  "    repeat = Math.max(repeat, r);\n" \
  "  });\n" \
  "  return repeat;\n" \
  "}\n"

// Check if string is palindrome.
#define JS_IsPalindrome \
  "  function IsPalindrome(s) {\n" \
  "    s = s.toLowerCase();\n" \
  "    return s == s.split('').reverse().join('');\n" \
  "  }\n"

// Disable specified form field. Also Disable a label, when all fileds in the row are disabled.
#define JS_SetFieldAvailability \
  "function SetFieldAvailability(field, enabled) {\n" \
  "  if (!field) {\n" \
  "    return;\n" \
  "  }\n" \
  "  field.disabled = !enabled;\n" \
  "  const tr = field.closest('tr');\n" \
  "  let disabled = !enabled;\n" \
  "  for (const fld of Array(...tr.getElementsByTagName('input'), ...tr.getElementsByTagName('select'),\n" \
  "                            ...tr.getElementsByTagName('textarea'), ...tr.getElementsByTagName('button'))) {\n" \
  "    if (fld.checkVisibility({visibilityProperty: true, contentVisibilityAuto: true, opacityProperty: true})) {\n" \
  "      disabled &= fld.disabled;\n" \
  "    }\n" \
  "  }\n" \
  "  tr.style.color = disabled ? '" WEB_COLOR_DISABLED "' : '';\n" \
  "}\n"

// Page end scripts

#define JS_Password \
  "function ShowHidePassword(e) {\n" \
  "  let field = e.target.parentNode.previousSibling;\n" \
  "  if (field.type == 'text') {\n" \
  "    field.type = 'password';\n" \
  "    e.target.src = '../../show.png';\n" \
  "  } else {\n" \
  "    field.type = 'text';\n" \
  "    e.target.src = '../../hide.png';\n" \
  "  }\n" \
  "}\n" \
  "for (const show of document.getElementsByClassName('pwd-show-hide')) {\n" \
  "  let img = show.firstChild;\n" \
  "  img.addEventListener('click', ShowHidePassword);\n" \
  "}\n"

#define JS_PwdIndicator \
  "function PasswordScore(pwd, name) {\n" \
  "  if (!pwd) return 0;\n" \
  "  let score = pwd.length - 4;\n" \
  "  if (IsPalindrome(pwd)) score /= 2;\n" \
  "  if (name && name.length > 2 && pwd.includes(name)) score -= name.length - 1;\n" \
  "  score -= (GetRepeat(pwd) - 1) >> 1;\n" \
  "  score = Math.round(score * (1 + (GetClass(pwd) - 1)*0.2));\n" \
  "  if (score < 1) score = 1;\n" \
  "  if (score > 16) score = 16;\n" \
  "  return score;\n" \
  "}\n" \
  "function ShowPasswordQuality(e) {\n" \
  "  let pwd = e.target.value;\n" \
  "  let name = e.target.attributes['data-name-field'];\n" \
  "  if (name) name = name.value;\n" \
  "  if (name) name = document.getElementById(name) || document.getElementsByName(name)[0];\n" \
  "  if (name) name = name.value;\n" \
  "  let indicator = e.target.nextSibling.nextSibling;\n" \
  "  let quality = PasswordScore(pwd, name);\n" \
  "  if (quality > " STRINGIFY(INDICATOR_STEPS) ") quality = " STRINGIFY(INDICATOR_STEPS) ";\n" \
  "  let i = 0;\n" \
  "  let color = ['#FF0000', '#FE9900', '#F4D918', '#12C612'][Math.floor((quality - 1)/4)];\n" \
  "  for (const level of indicator.children) {\n" \
  "    level.style.backgroundColor = (++i > quality) ? '" WEB_COLOR_INDICATOR "' : color;\n" \
  "  }\n" \
  "}\n" \
  "for (const indicator of document.getElementsByClassName('pwd-indicator')) {\n" \
  "  let field = indicator.previousSibling.previousSibling;\n" \
  "  field.addEventListener('input', ShowPasswordQuality);\n" \
  "  field.dispatchEvent(new Event('input'));\n" \
  "  let name = field.attributes['data-name-field'];\n" \
  "  if (name) name = name.value;\n" \
  "  if (name) name = document.getElementById(name) || document.getElementsByName(name)[0];\n" \
  "  if (name) name.addEventListener('input', ()=>field.dispatchEvent(new Event('input')));\n" \
  "}\n"

#define WEB_COLOR_INDICATOR "#E0E0E0"

#define INDICATOR_STEPS 16

// **************************************************************************

// flag indicating insertion of tag <form>
static unsigned int     um_table_form   = 0;

// current row in table
static unsigned int     um_table_row    = 0;

// current column in table
static unsigned int     um_table_col    = 0;

// number of columns in table
static unsigned int     um_table_cols   = 2;

// width of the first column in table
static unsigned int     um_table_align  = 0;

// ID for next cell in table
static const char       *um_table_id    = NULL;

// style for next cell in table
static const char       *um_table_style = NULL;

// flag indicating insertion of password field
static bool             um_pwd_field     = false;
// flag indicating insertion of password quality indicator
static bool             um_pwd_indicator = false;

// flag indicating insertion of password checking functions
static bool             um_pwd_classes    = false;
static bool             um_pwd_repeats    = false;
static bool             um_pwd_palindrome = false;

// **************************************************************************
// send HTML code of JavaScript
void um_html_js(const char *js, ...)
{
  const char   *nonce;
  va_list      args;

  nonce = um_cgi_get_nonce_js();

  // The type attribute is deprecated now, but we still use HTML 4.
  printf("<script%s%s%s type=\"text/javascript\">\n", *nonce ? " nonce=\"" : "", nonce, *nonce ? "\"" : "");

  va_start(args, js);
  vprintf(js, args);
  va_end(args);

  printf("</script>\n");
}

// **************************************************************************
// send HTML code of external JavaScript
void um_html_js_external(const char *filename)
{
  const char   *nonce;

  nonce = um_cgi_get_nonce_js();

  printf("<script%s%s%s src=\"%s\"></script>\n", *nonce ? " nonce=\"" : "", nonce, *nonce ? "\"" : "", filename);
}

// **************************************************************************
// send HTML code of Cascade Styles
void um_html_css(const char *css, ...)
{
  const char   *nonce;
  va_list      args;

  nonce = um_cgi_get_nonce_css();

  // The type attribute is deprecated now, but we still use HTML 4.
  printf("<style%s%s%s type=\"text/css\">\n", *nonce ? " nonce=\"" : "", nonce, *nonce ? "\"" : "");

  va_start(args, css);
  vprintf(css, args);
  va_end(args);

  printf("</style>\n");
}

// **************************************************************************
// send HTML code of beginning of page
void um_html_page_begin(const char *title)
{
  char   *session_timeout;
  // Define code style
  char *code_style =
      "* {\n"
      "  box-sizing: border-box;\n"
      "}\n"
      "body {\n"
      "  background-color: #FFFFFF;\n"
      "}\n"
      "body, table, tr, td, a {\n"
      "  font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif;\n"
      "}\n"
      "input, select, option, textarea, button {\n"
      "  font-size: 13.3333px;\n"
      "}\n"
      "form, input {\n"
      "  margin: 0px;\n"
      "}\n"
      "pre {\n"
      "  margin: 10px 0px 10px 0px;\n"
      "}\n"
      "h2 {\n"
      "  font-size: 24px; margin-bottom: 3px;\n"
      "}\n"
      "a {\n"
      "  color: " WEB_COLOR_PRIMARY "; text-decoration: none;\n"
      "}\n"
      "a:hover {\n"
      "  text-decoration: underline;\n"
      "}\n"
      ".logo {\n"
      "  color: " WEB_COLOR_PRIMARY "; text-shadow: " WEB_COLOR_SHADOW " 2px 2px 2px; filter: shadow(color=" WEB_COLOR_SHADOW ", direction=145, strength=4);\n"
      "}\n"
      ".window {\n"
      "  background-color: " WEB_COLOR_BACKGROUND "; border: 2px solid " WEB_COLOR_PRIMARY "; border-collapse: collapse;\n"
      "}\n"
      ".title {\n"
      "  color: " WEB_COLOR_TITLE "; background-color: " WEB_COLOR_PRIMARY "; font-weight: bold;\n"
      "}\n"
      ".header {\n"
      "  color: " WEB_COLOR_TEXT "; background-color: " WEB_COLOR_SECONDARY ";\n"
      "}\n"
      ".form-section {\n"
      "  position: relative;\n"
      "}\n"
      ".input, .input-common {\n"
      "  width: 175px;\n"
      "}\n"
      ".input-narrow {\n"
      "  width: 100px;\n"
      "}\n"
      ".input-wide {\n"
      "  width: 200px;\n"
      "}\n"
      ".input-large {\n"
      "  width: 580px;\n"
      "}\n"
      ".input-removed {\n"
      "  height: 18px;\n"
      "  padding-top: 2px;\n"
      "  color: #808080;\n"
      "}\n"
      ".password {\n"
      "  padding-right: 20px;\n"
      "}\n"
      ".box-common {\n"
      "  border-color: " WEB_COLOR_PRIMARY ";\n"
      "}\n"
      ".box-common > tbody > tr:first-child > td {\n"
      "  background-color: " WEB_COLOR_PRIMARY ";\n"
      "}\n"
      ".box-error {\n"
      "  border-color: " WEB_COLOR_ERROR ";\n"
      "}\n"
      ".box-error > tbody > tr:first-child > td {\n"
      "  background-color: " WEB_COLOR_ERROR ";\n"
      "}\n"
      ".box-success {\n"
      "  border-color: " WEB_COLOR_SUCCESS";\n"
      "}\n"
      ".box-success > tbody > tr:first-child > td {\n"
      "  background-color: " WEB_COLOR_SUCCESS ";\n"
      "}\n"
      ".box-common > tbody > tr:first-child > td, "
      ".box-error > tbody > tr:first-child > td, "
      ".box-success > tbody > tr:first-child > td {\n"
      "  text-align: center;\n"
      "  font-weight: bold;\n"
      "  color: " WEB_COLOR_TITLE ";\n"
      "}\n"
      ".text-error, .text-attention {\n"
      "  color: #FF0000;\n"
      "}\n"
      ".autocomplete_bypass {\n"
      "  display: none;\n"
      "}\n"
      ".foldable-list-item {\n"
      "  margin:4px 0px;\n"
      "}\n"
    ".invisible-element {\n"
    "  display: none;\n"
    "}\n"
    ".pwd-show-hide {\n"
    "  display: inline-block;\n"
    "  width: 0;\n"
    "  overflow: visible;\n"
    "  position: relative;\n"
    "  left: -20px;\n"
    "  top: 4px;\n"
    "}\n"
    ".pwd-show-hide img:hover {\n"
    "  cursor: pointer;\n"
    "}\n"
    "input:disabled + .pwd-show-hide {\n"
    "  display: none;\n"
    "}\n"
    ".pwd-indicator {\n"
    "  width: 175px;\n"
    "  height: 7px;\n"
    "  margin-top: 4px;\n"
    "}\n"
    ".pwd-indicator div {\n"
    "  display: inline-block;\n"
    "  height: 100%;\n"
    "  width: 10px;\n"
    "  margin-right: 1px;\n"
    "  background-color: " WEB_COLOR_INDICATOR ";\n"
    "}\n"
    ".pwd-indicator div:last-child {\n"
    "  margin-right: 0;\n"
    "}";

  printf("Content-Type: text/html\n"
         "Cache-Control: no-cache\n"
         "Pragma: no-cache\n"
         "\n"
         "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
         "<html>\n"
         "\n"
         "<head>\n"
         "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n"
         "<meta http-equiv=\"Content-Style-Type\" content=\"text/css\">\n"
         "<meta http-equiv=\"Content-Script-Type\" content=\"text/javascript\">\n"
         "<title>%s</title>\n",
         title);

  um_html_css(code_style);

  session_timeout = getenv("SESSION_TIMEOUT");
  um_html_js(JS_SessionTimeout, session_timeout, session_timeout);

  printf("</head>\n"
         "\n"
         "<body>\n");
}

// **************************************************************************
// send HTML code of beginning admin`s page
int um_html_admin_page_begin(const char *title, const char *cgi_name) {
  um_html_page_begin(title);

  if (!um_cgi_is_admin_role()) {
    um_html_config_info_box(0, 0, 0, cgi_name);
    um_html_page_end();
    um_cgi_end();
    return 0;
  }

  return 1;
}

// **************************************************************************
// send HTML code of end of page
void um_html_page_end(void)
{
  printf("</body>\n"
         "\n"
         "</html>\n");

  fflush(stdout);
}

// **************************************************************************
// send HTML code of info box (generic)
void um_html_info_box(int ok, const char *title, const char *text, const char *url, const char *button)
{
  printf("<center>\n"
         "<form name=\"f\" action=\"%s\" method=\"get\">\n"
         "<table id=\"html_info_box_table\" cellspacing=\"0\" cellpadding=\"5\" class=\"window box-%s\">\n"
         "  <tr>\n"
         "    <td>%s</td>\n"
         "  </tr>\n"
         "  <tr>\n"
         "    <td>\n"
         "      <table>\n"
         "        <tr align=\"center\">\n"
         "          <td>%s</td>\n"
         "        </tr>\n"
         "        <tr align=\"center\">\n"
         "          <td><input type=\"submit\" name=\"button\" value=\"%s\"></td>\n"
         "        </tr>\n"
         "      </table>\n"
         "    </td>\n"
         "  </tr>\n"
         "</table>\n"
         "</form>\n"
         "</center>\n",
         url, ok ? "success" : "error", title, text, button);

  um_html_js("document.f.button.focus();\n");
}

// **************************************************************************
// send HTML code of info box (configuration)
void um_html_config_info_box(int ok, int input_ok, int reboot, const char *url)
{
  const char            *msg;

  if (input_ok && ok) {
    if (reboot) {
      msg = "Configuration successfully updated. You must <a href=\"reboot_exec.cgi\">reboot</a> to apply changes.";
    } else {
      msg = "Configuration successfully updated.";
    }
  } else {
    if (input_ok) {
      msg = "Error during configuration update.";
    } else {
      msg = "Invalid input.";
    }
  }

  um_html_info_box(ok, "Configuration", msg, url, "Back");
}

// **************************************************************************
// send HTML code of beginning of form
void um_html_form_begin(const char *heading, const char *title, const char *action, int multipart, const char *javascript, const um_menu_item_t *menu)
{
  char   *request_id;

  static const um_menu_item_t DEFAULT_MENU[] = {
    { "Administration", NULL               },
    { "Return"        , "../../module.cgi" },
    { NULL            , NULL               }
  };

  if (!menu) {
    menu = DEFAULT_MENU;
  }

  if (javascript) {
    um_html_js("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
               strstr(javascript, "GetValue") || strstr(javascript, "IsEmpty") || strstr(javascript, "IsInRange") || strstr(javascript, "IsValid") ? JS_GetValue : "",
               strstr(javascript, "Error") ? JS_Error : "",
               strstr(javascript, "IsEmpty") ? JS_IsEmpty : "",
               strstr(javascript, "IsInRange") ? JS_IsInRange : "",
               strstr(javascript, "IsValidString") ? JS_IsValidString : "",
               strstr(javascript, "IsValidMAC") ? JS_IsValidMAC : "",
               strstr(javascript, "IsValidIP") ? JS_IsValidIP : "",
               strstr(javascript, "IsValidIP6") ? JS_IsValidIP6 : "",
               strstr(javascript, "IsValidPrefix") ? JS_IsValidPrefix : "",
               strstr(javascript, "IsValidPort") ? JS_IsValidPort : "",
               strstr(javascript, "ShowOption") ? JS_ShowOption : "",
               strstr(javascript, "HideOption") ? JS_HideOption : "",
               strstr(javascript, "GetClass") ? JS_GetClass : "",
               strstr(javascript, "GetRepeat") ? JS_GetRepeat : "",
               strstr(javascript, "IsPalindrome") ? JS_IsPalindrome : "",
               strstr(javascript, "SetFieldAvailability") ? JS_SetFieldAvailability : "",
               javascript);

    um_pwd_classes = strstr(javascript, "GetClass");
    um_pwd_repeats = strstr(javascript, "GetRepeat");
    um_pwd_palindrome = strstr(javascript, "IsPalindrome");
  }

  printf("<table width=\"100%%\" cellspacing=\"2\" cellpadding=\"2\" border=\"0\">\n"
         "  <tr>\n"
         "    <td nowrap class=\"logo\"><h2>%s</h2></td>\n"
         "  </tr>\n"
         "</table>\n"
         "<table width=\"100%%\" cellspacing=\"2\" cellpadding=\"2\" border=\"0\">\n"
         "  <tr>\n"
         "    <td valign=\"top\" width=\"1%%\">\n"
         "      <table width=\"200\" cellspacing=\"0\" cellpadding=\"5\" border=\"2\" class=\"window\">\n", heading);

  while (menu->label) {
    printf("        <tr>\n"
           "          <td nowrap class=\"title\">%s</td>\n"
           "        </tr>\n"
           "        <tr>\n"
           "          <td>\n"
           "            <table cellspacing=\"2\" cellpadding=\"1\">\n", menu->label);
    while ((++menu)->url) {
      printf("              <tr><td nowrap><a href=\"%s\">%s</a></td></tr>\n", menu->url, menu->label);
    }
    printf("            </table>\n"
           "          </td>\n"
           "        </tr>\n");
  }

  printf("      </table>\n"
         "    </td>\n"
         "    <td valign=\"top\" width=\"99%%\">\n");

  if (action) {
    printf("      <form name=\"f\" action=\"%s\" method=\"post\"", action);
    if (multipart) {
      printf(" enctype=\"multipart/form-data\">\n");
    } else {
      printf(">\n");
    }
    um_table_form = 1;
    request_id = um_csrf_gen_token();
    if (request_id) {
      printf("      <input type=\"hidden\" name=\"request_id\" value=\"%s\">\n", request_id);
    }
  }

  printf("      <table width=\"100%%\" cellspacing=\"0\" cellpadding=\"5\" border=\"2\" class=\"window\">\n"
         "        <tr>\n"
         "          <td align=\"center\" id=\"form-title\" class=\"title\">%s</td>\n"
         "        </tr>\n"
         "        <tr>\n"
         "          <td>", title);
}

// **************************************************************************
// send HTML code of end of form
void um_html_form_end(const char *javascript)
{
  if (um_table_col) {
    printf("</tr>");
  }
  if (um_table_row) {
    printf("</table>");
  }

  printf("</td>\n"
         "        </tr>\n"
         "      </table>\n");
  if (um_table_form) {
    printf("      </form>\n");
  }
  printf("    </td>\n"
         "  </tr>\n"
         "</table>\n");

  if (javascript || um_pwd_field || um_pwd_indicator) {
    um_html_js("%s%s%s%s%s%s",
               um_pwd_field ? JS_Password : "",
               um_pwd_indicator && !um_pwd_classes ? JS_GetClass : "",
               um_pwd_indicator && !um_pwd_repeats ? JS_GetRepeat : "",
               um_pwd_indicator && !um_pwd_palindrome ? JS_IsPalindrome : "",
               um_pwd_indicator ? JS_PwdIndicator : "",
               javascript ? javascript : "");
  }

  if (javascript) {
    um_html_js(javascript);
  }
}

// **************************************************************************
// send HTML code of horizontal line in form
void um_html_form_break(void)
{
  if (um_table_col) {
    printf("</tr>");
    um_table_col = 0;
  }
  if (um_table_row) {
    printf("</table>");
    um_table_row = 0;
  }

  printf("</td></tr><tr><td>");
}

// **************************************************************************
// define structure of table
void um_html_table(unsigned int cols, unsigned int align)
{
  if (um_table_col) {
    printf("</tr>");
    um_table_col = 0;
  }
  if (um_table_row) {
    printf("</table>");
    um_table_row = 0;
  }

  um_table_cols  = cols;
  um_table_align = align;
}

// **************************************************************************
// define ID for next cell in table
void um_html_id(const char *name)
{
  um_table_id = name;
}

// **************************************************************************
// define style for next cell in table
void um_html_style(const char *style)
{
  um_table_style = style;
}

// **************************************************************************
// send HTML code of beginning of cell
static void um_html_cell_begin(void)
{
  if (!um_table_row) {
    printf("<table>");
  }
  if (!um_table_col) {
    printf("<tr>");
    um_table_row++;
  }

  if (um_table_align && !um_table_col && um_table_row == 1) {
    if (um_table_id) {
      printf("<td nowrap width=\"%u\" id=\"%s\">", um_table_align, um_table_id);
    } else {
      printf("<td nowrap width=\"%u\">", um_table_align);
    }
  } else {
    if (um_table_id) {
      printf("<td nowrap id=\"%s\">", um_table_id);
    } else {
      printf("<td nowrap>");
    }
  }
  um_table_col++;

  um_table_id = NULL;

  if (!um_table_style) {
    um_table_style = "class=\"input\"";
  }
}

// **************************************************************************
// send HTML code of end of cell
static void um_html_cell_end(void)
{
  if (um_table_col < um_table_cols) {
    printf("</td>");
  } else {
    printf("</td></tr>");
    um_table_col = 0;
  }

  um_table_style = NULL;
}

// **************************************************************************
// send HTML code of simple text
void um_html_text(const char *text)
{
  um_html_cell_begin();

  printf("%s", text);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of variable simple text
void um_html_vtext(const char *format, ...)
{
  va_list args;

  um_html_cell_begin();

  va_start(args, format);
  vprintf(format, args);
  va_end(args);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of link
void um_html_link(const char *url, const char *text)
{
  um_html_cell_begin();

  printf("<a href=\"%s\">%s</a>", url, text);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (string)
void um_html_input_str(const char *name, const char *value)
{
  um_html_cell_begin();

  printf("<input %s type=\"text\" name=\"%s\" value=\"%s\">", um_table_style, name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (read only string)
void um_html_input_str_ro(const char *name, const char *value)
{
  um_html_cell_begin();

  printf("<input %s type=\"text\" name=\"%s\" value=\"%s\" disabled>", um_table_style, name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (hidden string)
void um_html_input_str_hd(const char *name, const char *value)
{
  um_html_cell_begin();

  printf("<input type=\"hidden\" name=\"%s\" value=\"%s\">", name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (integer)
void um_html_input_int(const char *name, unsigned int value, int show_zero, const char *unit)
{
  um_html_cell_begin();

  printf("<input %s type=\"text\" name=\"%s\" value=\"", um_table_style, name);
  if (value || show_zero) {
    printf("%u", value);
  }
  if (unit) {
    printf("\"> %s", unit);
  } else {
    printf("\">");
  }

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (hidden integer)
void um_html_input_int_hd(const char *name, unsigned int value)
{
  um_html_cell_begin();

  printf("<input type=\"hidden\" name=\"%s\" value=\"%u\">", name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (IP address)
void um_html_input_ip(const char *name, unsigned int value)
{
  um_html_cell_begin();

  printf("<input %s type=\"text\" name=\"%s\" value=\"", um_table_style, name);
  if (value) {
    printf("%u.%u.%u.%u\">",
           (value >> 24) & 0xFF,
           (value >> 16) & 0xFF,
           (value >> 8) & 0xFF,
           (value) & 0xFF);
  } else {
    printf("\">");
  }

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (IPv6 address)
void um_html_input_ip6(const char *name, const struct in6_addr *value)
{
  char                  ip6_str[INET6_ADDRSTRLEN];

  um_html_cell_begin();

  if (um_ip6_addr_equal(value, &in6addr_any) || !inet_ntop(AF_INET6, value, ip6_str, sizeof(ip6_str))) {
    ip6_str[0] = '\0';
  }
  printf("<input %s type=\"text\" name=\"%s\" value=\"%s\">", um_table_style, name, ip6_str);

  um_html_cell_end();
}

// *****************************************************************************
// send HTML code of input box (password with optional quality indicator)
void um_html_input_pwd(const char *name, const char *value, bool autocomplete, bool indicator, const char *name_field)
{
  char     *style;
  char     *s;
  size_t   len;

  um_html_cell_begin();

  // workaround for incomplete browser support for attribute autocomplete="off":
  // https://caniuse.com/input-autocomplete-onoff
  if (!autocomplete) {
    printf("<input type=\"text\" name=\"rememberedusername\" class=\"invisible-element\">"
           "<input type=\"password\" name=\"rememberedpassword\" class=\"invisible-element\">");
  }

  // we need assigned a "password" class, even when developer includes own classes via html_style()
  if (um_table_style) {
    len = strlen(um_table_style);
    if ((s =strstr(um_table_style, "class=\""))) {
      s += 7;
      if ((style = malloc(len + 10))) {
        len = s - um_table_style;
        memcpy(style, um_table_style, len);
        memcpy(style + len, "password ", 10);
        strcat(style, s);
      }
    } else {
      if ((style = malloc(len + 18))) {
        strcpy(style, um_table_style);
        strcat(style, " class=\"password\"");
      }
    }
  } else {
    style = strdup("class=\"password\"");
  }

  printf("<input %s type=\"password\" name=\"%s\" value=\"%s\" autocomplete=\"%s\"%s%s%s>",
         style ? style : "",
         name,
         value,
         autocomplete ? "on" : "off",
         name_field ? " data-name-field=\"" : "", name_field ? name_field : "", name_field ? "\"" : "");

  free(style);

  printf("<span class=\"pwd-show-hide\"><img src=\"../../show.png\" height=\"17\"></span>");

  um_pwd_field = true;

  if (indicator) {
    printf("<div class=\"pwd-indicator\">");
    for (unsigned int i = 0; i < INDICATOR_STEPS; i++) {
      printf("<div></div>");
    }
    printf("</div>");
    um_pwd_indicator = true;
  }

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of input box (file)
void um_html_input_file(const char *name)
{
  um_html_cell_begin();

  printf("<input type=\"file\" name=\"%s\" size=\"50\">", name);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of select box (string)
void um_html_select_str(const char *name, const char *value, const um_option_str_t *option_ptr)
{
  um_html_cell_begin();

  printf("<select %s name=\"%s\">", um_table_style, name);
  while (option_ptr->text) {
    printf("<option value=\"%s\"%s>%s", option_ptr->value, !strcmp(option_ptr->value, value) ? " selected" : "", option_ptr->text);
    option_ptr++;
  }
  printf("</select>");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of select box (integer)
void um_html_select_int(const char *name, unsigned int value, const um_option_int_t *option_ptr)
{
  um_html_cell_begin();

  printf("<select %s name=\"%s\">", um_table_style, name);
  while (option_ptr->text) {
    printf("<option value=\"%u\"%s>%s", option_ptr->value, option_ptr->value == value ? " selected" : "", option_ptr->text);
    option_ptr++;
  }
  printf("</select>");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of radio button
void um_html_radio(const char *name, unsigned int id, unsigned int value)
{
  um_html_cell_begin();

  printf("<input type=\"radio\" name=\"%s\" value=\"%u\"%s>", name, id, id == value ? " checked" : "");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of check box
void um_html_check_box(const char *name, unsigned int checked)
{
  um_html_cell_begin();

  printf("<input type=\"checkbox\" name=\"%s\"%s>", name, checked ? " checked" : "");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of text area (text)
void um_html_area_text(const char *name, const char *value, unsigned int cols, unsigned int rows)
{
  um_html_cell_begin();

  printf("<textarea name=\"%s\" cols=\"%u\" rows=\"%u\">\n%s</textarea>", name, cols, rows, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of text area (file)
void um_html_area_file(const char *name, const char *filename, unsigned int cols, unsigned int rows)
{
  FILE                  *file_ptr;
  int                   ch;

  um_html_cell_begin();

  printf("<textarea name=\"%s\" cols=\"%u\" rows=\"%u\">\n", name, cols, rows);
  if ((file_ptr = fopen(filename, "r"))) {
    while ((ch = fgetc(file_ptr)) != EOF) {
      switch (ch) {
        case '&':
          printf("&amp;");
          break;
        case '<':
          printf("&lt;");
          break;
        case '>':
          printf("&gt;");
          break;
        default:
          putchar(ch);
      }
    }
    fclose(file_ptr);
  }
  printf("</textarea>");

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of submit button
void um_html_submit(const char *name, const char *value)
{
  um_html_cell_begin();

  printf("<input type=\"submit\" name=\"%s\" value=\"%s\">", name, value);

  um_html_cell_end();
}

// **************************************************************************
// send HTML code of beginning of cell
static void um_html_pre_cell_begin(int header)
{
  if (!um_table_row) {
    printf("<table width=\"100%%\">");
    um_table_row++;
  }

  if (header) {
    if (um_table_id) {
      printf("<tr><td nowrap align=\"center\" class=\"header\" id=\"%s\">", um_table_id);
    } else {
      printf("<tr><td nowrap align=\"center\" class=\"header\">");
    }
  } else {
    if (um_table_id) {
      printf("<tr><td nowrap id=\"%s\"><pre>\n", um_table_id);
    } else {
      printf("<tr><td nowrap><pre>\n");
    }
  }

  um_table_id = NULL;
}

// **************************************************************************
// send HTML code of end of cell
static void um_html_pre_cell_end(int header)
{
  if (header) {
    printf("</td></tr>");
  } else {
    printf("</pre></td></tr>");
  }
}

// **************************************************************************
// send HTML code of header of preformatted block
void um_html_pre_head(const char *text)
{
  um_html_pre_cell_begin(1);

  printf("%s", text);

  um_html_pre_cell_end(1);
}

// **************************************************************************
// send HTML code of preformatted block (text)
void um_html_pre_text(const char *text)
{
  um_html_pre_cell_begin(0);

  printf("%s\n", text);

  um_html_pre_cell_end(0);
}

// **************************************************************************
// send HTML code of preformatted block (file)
// The entities arg specifies whether to convert special chars to HTML entities.
void um_html_pre_file(const char *filename, const char *error, bool entities)
{
  um_html_pre_cell_begin(0);

  um_http_file_content(filename, error, entities);

  um_html_pre_cell_end(0);
}

// **************************************************************************
// send HTML code of preformatted block (program output)
// The entities arg specifies whether to convert special chars to HTML entities.
void um_html_pre_proc(const char *program, bool entities)
{
  char     *program_with_filtering;
  size_t   len;

  um_html_pre_cell_begin(0);

  fflush(stdout);

  if (entities) {
    // with replaced characters '<', '>', '&'

    len = strlen(program) + sizeof(HTML_FILTER);
    program_with_filtering = malloc(len);

    if (program_with_filtering) {
      snprintf(program_with_filtering, len, "%s" HTML_FILTER, program);

      um_process_redirect_stdout(STDOUT_FILENO);
      um_process_exec("/bin/sh", "-c", program_with_filtering);

      free(program_with_filtering);
    }

  } else {
    // unchanged output

    um_process_redirect_stdout(STDOUT_FILENO);
    um_process_exec("/bin/sh", "-c", program);
  }

  um_html_pre_cell_end(0);
}

// **************************************************************************
// send HTML code of page with system log
void um_html_system_log(const char *title, const um_menu_item_t *menu)
{
  um_html_page_begin(title);

  um_html_form_begin(title, "System Log", "/slog_save.cgi", 0, NULL, menu);

  um_html_pre_head("System Messages");
  um_html_pre_proc("cat /var/log/messages.1 /var/log/messages 2> /dev/null | tail -n 25", true);

  um_html_form_break();

  um_html_table(2, 0);

  um_html_submit("button", "Save Log");
  um_html_submit("button", "Save Report");

  um_html_form_end(NULL);

  um_html_page_end();
}

// **************************************************************************
// send HTML code of detected invalid hash
void um_html_integrity(const char *config_path)
{
  if (!um_cfg_verify(config_path)) {
    um_html_table(1, 0);
    um_html_cell_begin();
    printf("<span class=\"text-error\">Invalid configuration hash detected! (%s)</span>", config_path);
    um_html_cell_end();
    um_html_form_break();
  }
}
