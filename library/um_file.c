// **************************************************************************
//
// Functions for work with files
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "um_file.h"

// **************************************************************************
// check existence of file
int um_file_exists(const char *filename)
{
  struct stat           sb;

  return stat(filename, &sb) == 0;
}

// **************************************************************************
// get timestamp of file
time_t um_file_time(const char *filename)
{
  struct stat           sb;

  return stat(filename, &sb) ? 0 : sb.st_mtime;
}

// **************************************************************************
// get size of file
off_t um_file_size(const char *filename)
{
  struct stat           sb;

  return stat(filename, &sb) ? 0 : sb.st_size;
}

// **************************************************************************
// get the first line of file
char *um_file_gets(const char *filename)
{
  FILE                  *file_ptr;
  static __thread char  buffer[256];

  *buffer = '\0';

  if ((file_ptr = fopen(filename, "r"))) {
    if (fgets(buffer, sizeof(buffer), file_ptr)) {
      strtok(buffer, "\r\n");
    }
    fclose(file_ptr);
  }

  return buffer;
}

// **************************************************************************
// put string to file
int um_file_puts(const char *filename, const char *str)
{
  FILE                  *file_ptr;

  if ((file_ptr = fopen(filename, "w"))) {
    fputs(str, file_ptr);
    fclose(file_ptr);
    return 1;
  }

  return 0;
}

