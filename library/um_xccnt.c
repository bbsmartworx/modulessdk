// **************************************************************************
//
// Functions for work with XC-CNT expansion board
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <string.h>
#include <unistd.h>
#include <sys/select.h>

#include "um_xccnt.h"
#include "um_com.h"
#include "um_lock.h"

// **************************************************************************

// register numbers
#define UM_XCCNT_FEATURES   0x0003
#define UM_XCCNT_BIN        0x0100
#define UM_XCCNT_OUT        0x0200
#define UM_XCCNT_IMPULSE    0x0201
#define UM_XCCNT_AN1        0x0300
#define UM_XCCNT_AN2        0x0400
#define UM_XCCNT_CNT1       0x0500
#define UM_XCCNT_CNT2       0x0600

// **************************************************************************
// put 1 byte into buffer
static void um_put_byte(char *buffer, int *len_ptr, int *sum_ptr, int data)
{
  const char *HEX = "0123456789ABCDEF";

  buffer[(*len_ptr)++] = HEX[(data >> 4) & 0x0F];
  buffer[(*len_ptr)++] = HEX[data & 0x0F];
  *sum_ptr -= data;
}

// **************************************************************************
// put 2 bytes into buffer
static void um_put_word(char *buffer, int *len_ptr, int *sum_ptr, int data)
{
  um_put_byte(buffer, len_ptr, sum_ptr, data >> 8);
  um_put_byte(buffer, len_ptr, sum_ptr, data);
}

// **************************************************************************
// get 1 byte from buffer
static int um_get_byte(const char *buffer, int pos)
{
  int                   data_hi, data_lo, ch;

  ch = buffer[pos];
  data_hi = (ch >= 'A') ? (ch - 'A' + 10) : (ch - '0');
  ch = buffer[pos + 1];
  data_lo = (ch >= 'A') ? (ch - 'A' + 10) : (ch - '0');

  return (data_hi << 4) + data_lo;
}

// **************************************************************************
// get 2 bytes from buffer
static int um_get_word(const char *buffer, int pos)
{
  return ((um_get_byte(buffer, pos) << 8) + um_get_byte(buffer, pos + 2));
}

// **************************************************************************
// send request and receive response
static const char *um_modbus_xmit(int port, const char *request)
{
  static __thread char  response[512];
  const char            *device;
  struct timeval        tv;
  fd_set                rfds;
  int                   fd, cnt, sum, i;
  char                  ch;

  // prepare result
  memset(response, 0, sizeof(response));

  // select serial port
  device = port ? "/dev/ttyS1" : "/dev/ttyS0";

  // lock serial port
  if (!um_lock_acquire(device, 5)) {
    return response;
  }

  // open serial port
  if ((fd = um_com_open(device, 9600, 8, "N", 1)) < 0) {
    um_lock_release(device);
    return response;
  }

  // send request
  if (write(fd, request, strlen(request)) < 0) {
    close(fd);
    um_lock_release(device);
    return response;
  }

  // receive response
  cnt        = 0;
  tv.tv_sec  = 0;
  tv.tv_usec = 200000;
  while (1) {
    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    if (select(fd + 1, &rfds, NULL, NULL, &tv) <= 0) {
      break;
    }
    if (read(fd, &ch, sizeof(ch)) <= 0) {
      break;
    }
    if (ch == ':' || cnt) {
      response[cnt++] = ch;
    }
    if (ch == '\n' || cnt == sizeof(response) - 1) {
      break;
    }
    tv.tv_sec  = 0;
    tv.tv_usec = 20000;
  }

  // calculate checksum
  for (sum = 0, i = 1; i < cnt - 2; i += 2) {
    sum += um_get_byte(response, i);
  }

  // discard response if checksum is wrong
  if (sum & 0xFF) {
    response[0] = '\0';
  }

  // close serial port
  close(fd);

  // unlock serial port
  um_lock_release(device);

  // return received response
  return response;
}

// **************************************************************************
// read registers
static int um_modbus_read(int port, int rn, int wc, unsigned short *data_ptr)
{
  const char            *response;
  char                  request[32];
  int                   result, len, sum, i;

  // prepare result
  result = 0;

  // prepare request
  len = 0; sum = 0;
  request[len++] = ':';
  um_put_byte(request, &len, &sum, 0x00);
  um_put_byte(request, &len, &sum, 0x03);
  um_put_byte(request, &len, &sum, rn >> 8);
  um_put_byte(request, &len, &sum, rn);
  um_put_byte(request, &len, &sum, wc >> 8);
  um_put_byte(request, &len, &sum, wc);
  um_put_byte(request, &len, &sum, sum);
  request[len++] = '\r';
  request[len++] = '\n';
  request[len++] = '\0';

  // read registers
  for (i = 0; i < 3; i++) {
    // send request and receive response
    response = um_modbus_xmit(port, request);
    // if expected response was received
    if (strlen(response) == (size_t)(11 + 4 * wc)) {
      // process response
      for (i = 0; i < wc; i++) {
        *data_ptr++ = (unsigned short)um_get_word(response, 4 * i + 7);
      }
      // abort cycle
      result = 1;
      break;
    }
  }

  // return result of operation
  return result;
}

// **************************************************************************
// write registers
static int um_modbus_write(int port, int rn, int wc, unsigned short *data_ptr)
{
  const char            *response;
  char                  request[32];
  int                   result, len, sum, i;

  // prepare result
  result = 0;

  // prepare request
  len = 0; sum = 0;
  request[len++] = ':';
  um_put_byte(request, &len, &sum, 0x00);
  um_put_byte(request, &len, &sum, 0x10);
  um_put_byte(request, &len, &sum, rn >> 8);
  um_put_byte(request, &len, &sum, rn);
  um_put_byte(request, &len, &sum, wc >> 8);
  um_put_byte(request, &len, &sum, wc);
  um_put_byte(request, &len, &sum, wc << 1);
  for (i = 0; i < wc; i++) {
    um_put_word(request, &len, &sum, *data_ptr++);
  }
  um_put_byte(request, &len, &sum, sum);
  request[len++] = '\r';
  request[len++] = '\n';
  request[len++] = '\0';

  // write registers
  for (i = 0; i < 5; i++) {
    // send request and receive response
    response = um_modbus_xmit(port, request);
    // if expected response was received
    if (strlen(response) == 17) {
      // abort cycle
      result = 1;
      break;
    }
  }

  // return result of operation
  return result;
}

// **************************************************************************
// get bitmask of supported features
int um_xccnt_get_features(int port, unsigned short *value_ptr)
{
  unsigned short        data;
  int                   result;

  // read register
  result = um_modbus_read(port, UM_XCCNT_FEATURES, 1, &data);

  // decode data
  *value_ptr = (unsigned short)(result ? data : 0);

  // return result of operation
  return result;
}

// **************************************************************************
// get state of binary inputs
int um_xccnt_get_inputs(int port, unsigned short *value_ptr)
{
  unsigned short        data;
  int                   result;

  // read register
  result = um_modbus_read(port, UM_XCCNT_BIN, 1, &data);

  // decode data
  *value_ptr = (unsigned short)(result ? data : 0);

  // return result of operation
  return result;
}

// **************************************************************************
// get state of binary input
int um_xccnt_get_input(int port, int idx, unsigned short *value_ptr)
{
  unsigned short        data;
  int                   result;

  // read register
  result = um_modbus_read(port, UM_XCCNT_BIN, 1, &data);

  // decode data
  *value_ptr = (unsigned short)(result ? ((data & (1 << idx)) ? 1 : 0) : 0);

  // return result of operation
  return result;
}

// **************************************************************************
// get state of analog input
int um_xccnt_get_analog(int port, int idx, unsigned short *value_ptr)
{
  unsigned short        data;
  int                   result;

  // read register
  if (idx == 0) {
    result = um_modbus_read(port, UM_XCCNT_AN1, 1, &data);
  } else if (idx == 1) {
    result = um_modbus_read(port, UM_XCCNT_AN2, 1, &data);
  } else {
    result = 0;
  }

  // decode data
  *value_ptr = (unsigned short)(result ? data : 0);

  // return result of operation
  return result;
}

// **************************************************************************
// get state of counter input
int um_xccnt_get_counter(int port, int idx, unsigned int *value_ptr)
{
  unsigned short        data[2];
  int                   result;

  // read registers
  if (idx == 0) {
    result = um_modbus_read(port, UM_XCCNT_CNT1, 2, data);
  } else if (idx == 1) {
    result = um_modbus_read(port, UM_XCCNT_CNT2, 2, data);
  } else {
    result = 0;
  }

  // decode data
  *value_ptr = result ? (((unsigned int)data[0] << 16) + data[1]) : 0;

  // return result of operation
  return result;
}

// **************************************************************************
// set state of counter input
int um_xccnt_set_counter(int port, int idx, unsigned int value)
{
  unsigned short        data[2];

  // prepare data
  data[0] = (unsigned short)(value >> 16);
  data[1] = (unsigned short)(value & 0xFFFF);

  // write registers
  if (idx == 0) {
    return um_modbus_write(port, UM_XCCNT_CNT1, 2, data);
  } else if (idx == 1) {
    return um_modbus_write(port, UM_XCCNT_CNT2, 2, data);
  } else {
    return 0;
  }
}

// **************************************************************************
// get state of binary output
int um_xccnt_get_output(int port, unsigned short *value_ptr)
{
  unsigned short        data;
  int                   result;

  // read register
  result = um_modbus_read(port, UM_XCCNT_OUT, 1, &data);

  // decode data
  *value_ptr = (unsigned short)(result ? (data & 0x01) : 0);

  // return result of operation
  return result;
}

// **************************************************************************
// set state of binary output
int um_xccnt_set_output(int port, unsigned short value)
{
  unsigned short        data;

  // prepare data
  data = (unsigned short)(value ? 1 : 0);

  // write register
  return um_modbus_write(port, UM_XCCNT_OUT, 1, &data);
}

// **************************************************************************
// create impulse on binary output
int um_xccnt_impulse(int port, unsigned short cycles)
{
  // write register
  return um_modbus_write(port, UM_XCCNT_IMPULSE, 1, &cycles);
}

