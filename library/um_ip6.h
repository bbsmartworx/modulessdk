// **************************************************************************
//
// Functions for work with IPv6
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_IP6_H_
#define _UM_IP6_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <arpa/inet.h>

// comparison of two IPv6 addresses
extern int um_ip6_addr_equal(const struct in6_addr *ip6_1, const struct in6_addr *ip6_2);

#ifdef __cplusplus
}
#endif

#endif