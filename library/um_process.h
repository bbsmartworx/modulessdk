// **************************************************************************
//
// Functions for work with processes
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_PROCESS_H_
#define _UM_PROCESS_H_

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

// redirect stdin for the next executed process
extern void um_process_redirect_stdin(int fd);

// redirect stdout for the next executed process
extern void um_process_redirect_stdout(int fd);

// redirect stderr for the next executed process
extern void um_process_redirect_stderr(int fd);

// start a process and wait for its completion
extern int um_process_exec(const char *program, ...);

// start a process in the background
extern pid_t um_process_start(const char *program, ...);

// send a signal to process running in the background
extern int um_process_kill(pid_t pid, int sig);

// check existence of process running in the background
extern int um_process_exists(pid_t pid);

// macros for adding NULL after the last parameter
#define um_process_exec(program, ...) um_process_exec(program, ##__VA_ARGS__, NULL)
#define um_process_start(program, ...) um_process_start(program, ##__VA_ARGS__, NULL)

#ifdef __cplusplus
}
#endif

#endif

