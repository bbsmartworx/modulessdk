// **************************************************************************
//
// Functions for work with files
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_FILE_H_
#define _UM_FILE_H_

#include <time.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

// check existence of file
extern int um_file_exists(const char *filename);

// get timestamp of file
extern time_t um_file_time(const char *filename);

// get size of file
extern off_t um_file_size(const char *filename);

// get the first line of file
extern char *um_file_gets(const char *filename);

// put string to file
extern int um_file_puts(const char *filename, const char *str);

#ifdef __cplusplus
}
#endif

#endif

