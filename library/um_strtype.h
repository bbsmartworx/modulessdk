// **************************************************************************
//
// String manipulation library
//
// Copyright (C) 2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_STRTYPE_H_
#define _UM_STRTYPE_H_

// Decodes C string from an unprefixed, unseparated hexadecimally encoded C string.
// NULL is returned in case of memory allocation error.
char *um_hex2str(const char *hex_str);

// Encodes C string into an unprefixed, unseparated hexadecimally encoded C string.
// NULL is returned in case of memory allocation error.
char *um_str2hex(const char *str);

// Removes period (.) and colon (:) separators from a valid C-string-encoded
// hexadecimal value. Empty hexadecimal groups of length other than 2 are
// not supported.
// NULL is returned in case of memory allocation error.
char *um_hex_remove_separators(const char *in_str);

// Replaces all occurrences of the character in the string.
extern void um_char_replace(char *str, char old_char, char new_char);

// Replaces all occurrences of the substring in the string.
// Caller must provide memory block of sufficient size for all replacements, otherwise
// the string is truncated.
extern void um_substr_replace(char *str, char *old_substr, char *new_substr, size_t bufsize);

// Counts occurrences of the substring in the string.
extern unsigned int um_substr_occurrences(char *haystack, char *needle);


#endif // _UM_STRTYPE_H_
