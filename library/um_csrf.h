// **************************************************************************
//
// Soubor : um_csrf.h
// Autor  : Jan Mazura, Marek Černocký
// Popis  : CSRF attack protection
//
// **************************************************************************

#ifndef _UM_CSRF_H_
#define _UM_CSRF_H_

#ifdef __cplusplus
extern "C" {
#endif

// generate token
extern char *um_csrf_gen_token(void);

// check token
extern int um_csrf_check_token(const char *token);

#ifdef __cplusplus
}
#endif

#endif

