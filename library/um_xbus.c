// **************************************************************************
//
// Functions for communication via interprocess bus
//
// Copyright (C) 2020-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "um_xbus.h"
#include "um_file.h"

// **************************************************************************

// name of UNIX socket
#define XBUS_SOCKET     "/var/run/xbus.socket"

// maximum packet size
#define XBUS_MAX_SIZE   8192

// **************************************************************************

// socket descriptor
static int xbus_sk = -1;

// **************************************************************************
// concatenate multiple strings
static void concat(char *dst, size_t size, ...)
{
  va_list               ap;
  const char            *src;

  // copy source strings to target string
  va_start(ap, size);
  while ((src = va_arg(ap, const char *))) {
    while (*src && size > 1) {
      *dst++ = *src++;
      size--;
    }
  }
  va_end(ap);

  // terminate target string
  *dst = '\0';
}

// **************************************************************************
// check message broker availability
int um_xbus_available(void)
{
  // check existence of UNIX socket
  return um_file_exists(XBUS_SOCKET);
}

// **************************************************************************
// connect to message broker
void um_xbus_connect(void)
{
  struct sockaddr_un    addr;

  // return if connection is already established
  if (xbus_sk >= 0) {
    return;
  }

  // create new socket
  if ((xbus_sk = socket(AF_UNIX, SOCK_SEQPACKET, 0)) < 0) {
    syslog(LOG_CRIT, "xbus: create socket error: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // connect to server
  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, XBUS_SOCKET, sizeof(addr.sun_path) - 1);
  if (connect(xbus_sk, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
    syslog(LOG_CRIT, "xbus: connect socket error: %s", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // enable automatic socket close during successful exec
  fcntl(xbus_sk, F_SETFD, FD_CLOEXEC);
}

// **************************************************************************
// disconnect from message broker
void um_xbus_disconnect(void)
{
  char                  buffer[XBUS_MAX_SIZE];

  // return if connection is not established
  if (xbus_sk < 0) {
    return;
  }

  // disallow further transmissions
  shutdown(xbus_sk, SHUT_WR);

  // receive remaining packets from server
  while (recv(xbus_sk, buffer, sizeof(buffer), MSG_NOSIGNAL) > 0)
    ;

  // close socket
  close(xbus_sk);

  // invalidate socket descriptor
  xbus_sk = -1;
}

// **************************************************************************
// send packet to message broker
static void um_xbus_send(const char *command, const char *topic, const char *payload)
{
  char                  buffer[XBUS_MAX_SIZE];

  // connect to message broker
  um_xbus_connect();

  // create packet content
  concat(buffer, sizeof(buffer), command, " ", topic, "\n", payload, NULL);

  // send packet to message broker
  if (send(xbus_sk, buffer, strlen(buffer) + 1, MSG_EOR | MSG_NOSIGNAL) < 0) {
    syslog(LOG_CRIT, "xbus: connection terminated");
    exit(EXIT_FAILURE);
  }
}

// **************************************************************************
// subscribe to particular topic
void um_xbus_subscribe(const char *topic)
{
  // send packet SUBSCRIBE
  um_xbus_send("SUBSCRIBE", topic, "");
}

// **************************************************************************
// unsubscribe from particular topic
void um_xbus_unsubscribe(const char *topic)
{
  // send packet UNSUBSCRIBE
  um_xbus_send("UNSUBSCRIBE", topic, "");
}

// **************************************************************************
// publish message
void um_xbus_publish(const char *topic, const char *payload)
{
  // send packet PUBLISH
  um_xbus_send("PUBLISH", topic, payload);
}

// **************************************************************************
// publish and store message
void um_xbus_write(const char *topic, const char *payload)
{
  // send packet WRITE
  um_xbus_send("WRITE", topic, payload);
}

// **************************************************************************
// read stored message
char *um_xbus_read(const char *topic)
{
  // send packet READ
  um_xbus_send("READ", topic, "");

  // return received response
  return um_xbus_receive(NULL);
}

// **************************************************************************
// get list of stored messages
char *um_xbus_list(void)
{
  // send packet LIST
  um_xbus_send("LIST", "*", "");

  // return received response
  return um_xbus_receive(NULL);
}

// **************************************************************************
// receive message
char *um_xbus_receive(char **topic)
{
  static char           buffer[XBUS_MAX_SIZE];
  char                  *ptr;

  // connect to message broker
  um_xbus_connect();

  // receive packet from message broker
  if (recv(xbus_sk, buffer, sizeof(buffer), MSG_WAITALL | MSG_NOSIGNAL) <= 0) {
    syslog(LOG_CRIT, "xbus: connection terminated");
    exit(EXIT_FAILURE);
  }

  // split packet content
  buffer[sizeof(buffer) - 1] = '\0';
  ptr = strchrnul(buffer, '\n');
  if (*ptr) {
    *ptr++ = '\0';
  }

  // return message topic
  if (topic) {
    *topic = buffer;
  }

  // return message text
  return ptr;
}

// **************************************************************************
// check pending unread messages
int um_xbus_pending(void)
{
  struct pollfd         pfd;

  // prepare structure content
  pfd.fd     = xbus_sk;
  pfd.events = POLLIN;

  // detect presence of unread data
  return poll(&pfd, 1, 0) > 0;
}

// **************************************************************************
// get socket descriptor
int um_xbus_socket(void)
{
  // connect to message broker
  um_xbus_connect();

  // return socket descriptor
  return xbus_sk;
}

