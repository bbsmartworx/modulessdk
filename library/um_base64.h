// **************************************************************************
//
// Functions for BASE64 encoding/decoding
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_BASE64_H_
#define _UM_BASE64_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

// encode BASE64
extern void um_base64_encode(const unsigned char *src, char *dst, size_t size);

// decode BASE64
extern size_t um_base64_decode(const char *src, unsigned char *dst);

// encode BASE64 (result shall be freed by the caller)
extern char *um_base64_encode_str(const char *str);

// decode BASE64 (result shall be freed by the caller)
extern char *um_base64_decode_str(const char *str);

#ifdef __cplusplus
}
#endif

#endif

