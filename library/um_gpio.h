// **************************************************************************
//
// Functions for work with GPIO driver
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_GPIO_H_
#define _UM_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif

// types of expansion ports
#define UM_GPIO_PORT_TYPE_EMPTY             0x00
#define UM_GPIO_PORT_TYPE_RS232             0x02
#define UM_GPIO_PORT_TYPE_RS485             0x03
#define UM_GPIO_PORT_TYPE_MBUS              0x04
#define UM_GPIO_PORT_TYPE_CNT               0x05
#define UM_GPIO_PORT_TYPE_ETH               0x08
#define UM_GPIO_PORT_TYPE_WMBUS             0x0A
#define UM_GPIO_PORT_TYPE_RS422             0x0B
#define UM_GPIO_PORT_TYPE_NONE              0x0F
#define UM_GPIO_PORT_TYPE_WIFI              0x11
#define UM_GPIO_PORT_TYPE_SDCARD            0x12
#define UM_GPIO_PORT_TYPE_DUST              0x13
#define UM_GPIO_PORT_TYPE_SL869             0x14
#define UM_GPIO_PORT_TYPE_SWITCH            0x20
#define UM_GPIO_PORT_TYPE_DUST2             0x21

// types of module boards
#define UM_GPIO_MODULE_TYPE_EES3            0x00
#define UM_GPIO_MODULE_TYPE_EU3             0x01
#define UM_GPIO_MODULE_TYPE_MCXXXX_VWM10    0x02
#define UM_GPIO_MODULE_TYPE_PHS8            0x03
#define UM_GPIO_MODULE_TYPE_MCXXXX          0x04
#define UM_GPIO_MODULE_TYPE_VWM10           0x05
#define UM_GPIO_MODULE_TYPE_GOBI3K          0x06
#define UM_GPIO_MODULE_TYPE_LE910           0x07
#define UM_GPIO_MODULE_TYPE_EC25            0x08
#define UM_GPIO_MODULE_TYPE_MCXXXX_MCXXXX   0x0A
#define UM_GPIO_MODULE_TYPE_TRM             0x0E
#define UM_GPIO_MODULE_TYPE_NONE            0x0F

// get index of selected module
extern int um_gpio_get_module_idx(void);

// get type of module board
extern int um_gpio_get_module_type(int module);

// get index of selected SIM card
extern int um_gpio_get_module_sim(int module);

// set state of LED USR
extern void um_gpio_set_led_usr(unsigned int state);

// get type of expansion port
extern int um_gpio_get_port_type(int port);

// set shutdown of expansion port
extern void um_gpio_set_port_sd(int port, unsigned int state);

// get shutdown of expansion port
extern int um_gpio_get_port_sd(int port);

// set state of output on expansion board
extern void um_gpio_set_port_output(int port, unsigned int idx, unsigned int state);

// get state of input on expansion board
extern int um_gpio_get_port_input(int port, unsigned int idx);

// get information about overload on MBUS
extern int um_gpio_get_mbus_overload(int port);

// set state of output OUT0
extern void um_gpio_set_out0(unsigned int state);

// set state of output OUT1
extern void um_gpio_set_out1(unsigned int state);

// get state of output OUT0
extern int um_gpio_get_out0(void);

// get state of output OUT1
extern int um_gpio_get_out1(void);

// get state of input BIN0
extern int um_gpio_get_bin0(void);

// get state of input BIN1
extern int um_gpio_get_bin1(void);

// get state of input BIN2
extern int um_gpio_get_bin2(void);

// get state of input BIN3
extern int um_gpio_get_bin3(void);

// get availability of output OUT0
extern int um_gpio_has_out0(void);

// get availability of output OUT1
extern int um_gpio_has_out1(void);

// get availability of input BIN0
extern int um_gpio_has_bin0(void);

// get availability of input BIN1
extern int um_gpio_has_bin1(void);

// get availability of input BIN2
extern int um_gpio_has_bin2(void);

// get availability of input BIN3
extern int um_gpio_has_bin3(void);

// get internal temperature
extern int um_gpio_get_temperature(void);

// get supply voltage
extern int um_gpio_get_voltage(void);

#ifdef __cplusplus
}
#endif

#endif

