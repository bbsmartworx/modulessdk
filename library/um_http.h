// **************************************************************************
//
// Functions for sending file via HTTP protocol
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_HTTP_H_
#define _UM_HTTP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

// start file transfer
extern void um_http_file_begin(const char *filename);

// send content of file; the entities arg specifies whether to convert special chars to HTML entities
extern void um_http_file_content(const char *filename, const char *error, bool entities);

// finish file transfer
extern void um_http_file_end(void);

#ifdef __cplusplus
}
#endif

#endif

