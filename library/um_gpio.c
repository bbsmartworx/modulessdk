// **************************************************************************
//
// Functions for work with GPIO driver
//
// Copyright (C) 2023-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <fcntl.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

#include "um_gpio.h"
#include "um_process.h"

// **************************************************************************

// GPIO driver ioctl command codes
#define UM_GPIO_GET_MO1_SIM         0x80004202U
#define UM_GPIO_SET_LED_USR         0x40004203U
#define UM_GPIO_SET_OUT0            0x40004206U
#define UM_GPIO_GET_OUT0            0x80004206U
#define UM_GPIO_GET_BIN0            0x80004207U
#define UM_GPIO_GET_PORT1_TYPE      0x80004209U
#define UM_GPIO_GET_PORT1_OVRL      0x8000420AU
#define UM_GPIO_GET_PORT2_TYPE      0x8000420BU
#define UM_GPIO_GET_PORT2_OVRL      0x8000420CU
#define UM_GPIO_GET_MO1_TYPE        0x8000420EU
#define UM_GPIO_GET_TEMPERATURE     0x80004211U
#define UM_GPIO_GET_VOLTAGE         0x80004212U
#define UM_GPIO_SET_PORT1_SD        0x40004214U
#define UM_GPIO_GET_PORT1_SD        0x80004214U
#define UM_GPIO_SET_PORT2_SD        0x40004215U
#define UM_GPIO_GET_PORT2_SD        0x80004215U
#define UM_GPIO_GET_MO2_SIM         0x80004216U
#define UM_GPIO_GET_MO2_TYPE        0x80004219U
#define UM_GPIO_GET_MOD_IDX         0x8000421AU
#define UM_GPIO_GET_BIN1            0x8000421BU
#define UM_GPIO_GET_PORT1_IN        0x80004224U
#define UM_GPIO_SET_PORT1_OUT_LOW   0x40004225U
#define UM_GPIO_SET_PORT1_OUT_HIGH  0x40004226U
#define UM_GPIO_GET_PORT2_IN        0x80004227U
#define UM_GPIO_SET_PORT2_OUT_LOW   0x40004228U
#define UM_GPIO_SET_PORT2_OUT_HIGH  0x40004229U
#define UM_GPIO_SET_OUT1            0x4000422BU
#define UM_GPIO_GET_OUT1            0x8000422BU
#define UM_GPIO_GET_BIN2            0x8000422EU
#define UM_GPIO_GET_BIN3            0x8000422FU

// **************************************************************************
// get availability of GPIO pin on target HW if supported by GPIO driver
// workaround for bug report UMO-737
typedef enum {
  PIN_SUPPORT_UNKNOWN = 0,
  PIN_SUPPORTED,
  PIN_UNSUPPORTED
} pin_support_t;

typedef struct {
  const char* name;
  pin_support_t support;
} pin_support_table_entry;

typedef struct {
  pin_support_table_entry bin0;
  pin_support_table_entry bin1;
  pin_support_table_entry bin2;
  pin_support_table_entry bin3;
  pin_support_table_entry out0;
  pin_support_table_entry out1;
} pin_support_table;

static pin_support_table pin_support = {
  .bin0 = { "bin0", PIN_SUPPORT_UNKNOWN },
  .bin1 = { "bin1", PIN_SUPPORT_UNKNOWN },
  .bin2 = { "bin2", PIN_SUPPORT_UNKNOWN },
  .bin3 = { "bin3", PIN_SUPPORT_UNKNOWN },
  .out0 = { "out0", PIN_SUPPORT_UNKNOWN },
  .out1 = { "out1", PIN_SUPPORT_UNKNOWN }
};

// get availability of GPIO pin on target HW if supported by GPIO driver
static int um_gpio_init_pin_and_get_binary_value(pin_support_table_entry *pin)
{
  int result;
  int count;
  int fds[2];
  char buffer;

  if (pipe(fds) != 0) {
    syslog(LOG_ERR, "gpio: cannot create pipe when checking support for %s", pin->name);
    return -1;
  }

  um_process_redirect_stdout(fds[1]);
  result = um_process_exec("/usr/bin/io", "get", pin->name, NULL);

  // If the pin is not supported, the I/O applet returns 0 and prints
  // help or returns a different number without printing anything.
  // Otherwise, it prints value of the pin (0 or 1) and returns it.
  count = read(fds[0], &buffer, sizeof(buffer));
  close(fds[0]);

  if (count < 0) {
    syslog(LOG_ERR, "gpio: read error when checking support for %s", pin->name);
    return -1;
  } else if (count > 0 && (buffer == '0' || buffer == '1')) {
    pin->support = PIN_SUPPORTED;
    return WEXITSTATUS(result);
  } else {
    pin->support = PIN_UNSUPPORTED;
    return -1;
  }
}

// **************************************************************************
// pass command to GPIO driver
static int um_gpio_ioctl(unsigned int request, unsigned int state)
{
  static volatile int   fd   = -1;
  static volatile int   lock = 0;

  while (__sync_lock_test_and_set(&lock, 1));

  if (fd < 0) {
    fd = open("/dev/gpio", O_RDWR);
  }

  __sync_lock_release(&lock);

  if (fd < 0) {
    return -1;
  } else {
    return ioctl(fd, request, state);
  }
}

// **************************************************************************
// get index of selected module
int um_gpio_get_module_idx(void)
{
  int result = um_gpio_ioctl(UM_GPIO_GET_MOD_IDX, 0);
  return result >= 0 ? result : 0;
}

// **************************************************************************
// get type of module board
int um_gpio_get_module_type(int module)
{
  int result = um_gpio_ioctl(module ? UM_GPIO_GET_MO2_TYPE : UM_GPIO_GET_MO1_TYPE, 0);
  return result >= 0 ? result : UM_GPIO_MODULE_TYPE_NONE;
}

// **************************************************************************
// get index of selected SIM card
int um_gpio_get_module_sim(int module)
{
  int result = um_gpio_ioctl(module ? UM_GPIO_GET_MO2_SIM : UM_GPIO_GET_MO1_SIM, 0);
  return result >= 0 ? result : 0;
}

// **************************************************************************
// set state of LED USR
void um_gpio_set_led_usr(unsigned int state)
{
  um_gpio_ioctl(UM_GPIO_SET_LED_USR, state);
}

// **************************************************************************
// get type of expansion port
int um_gpio_get_port_type(int port)
{
  int result = um_gpio_ioctl(port ? UM_GPIO_GET_PORT2_TYPE : UM_GPIO_GET_PORT1_TYPE, 0);
  return result >= 0 ? result : UM_GPIO_PORT_TYPE_NONE;
}

// **************************************************************************
// set shutdown of expansion port
void um_gpio_set_port_sd(int port, unsigned int state)
{
  um_gpio_ioctl(port ? UM_GPIO_SET_PORT2_SD : UM_GPIO_SET_PORT1_SD, state);
}

// **************************************************************************
// get shutdown of expansion port
int um_gpio_get_port_sd(int port)
{
  int result = um_gpio_ioctl(port ? UM_GPIO_GET_PORT2_SD : UM_GPIO_GET_PORT1_SD, 0);
  return result >= 0 ? result : 0;
}

// **************************************************************************
// set state of output on expansion board
void um_gpio_set_port_output(int port, unsigned int idx, unsigned int state)
{
  um_gpio_ioctl(port ? (state ? UM_GPIO_SET_PORT2_OUT_HIGH : UM_GPIO_SET_PORT2_OUT_LOW) :
                       (state ? UM_GPIO_SET_PORT1_OUT_HIGH : UM_GPIO_SET_PORT1_OUT_LOW), idx);
}

// **************************************************************************
// get state of input on expansion board
int um_gpio_get_port_input(int port, unsigned int idx)
{
  int result = um_gpio_ioctl(port ? UM_GPIO_GET_PORT2_IN : UM_GPIO_GET_PORT1_IN, idx);
  return result >= 0 ? result : 0;
}

// **************************************************************************
// get information about overload on MBUS
int um_gpio_get_mbus_overload(int port)
{
  int result = um_gpio_ioctl(port ? UM_GPIO_GET_PORT2_OVRL : UM_GPIO_GET_PORT1_OVRL, 0);
  return result >= 0 ? result : 0;
}

// **************************************************************************
// set state of output OUT0
void um_gpio_set_out0(unsigned int state)
{
  if (pin_support.out0.support == PIN_SUPPORTED) {
    um_gpio_ioctl(UM_GPIO_SET_OUT0, state);
  } else if (pin_support.out0.support == PIN_UNSUPPORTED) {
    return;
  } else {
    um_gpio_init_pin_and_get_binary_value(&pin_support.out0);
    if (pin_support.out0.support == PIN_SUPPORTED) {
      um_gpio_ioctl(UM_GPIO_SET_OUT0, state);
    }
  }
}

// **************************************************************************
// set state of output OUT1
void um_gpio_set_out1(unsigned int state)
{
  if (pin_support.out1.support == PIN_SUPPORTED) {
    um_gpio_ioctl(UM_GPIO_SET_OUT1, state);
  } else if (pin_support.out1.support == PIN_UNSUPPORTED) {
    return;
  } else {
    um_gpio_init_pin_and_get_binary_value(&pin_support.out1);
    if (pin_support.out1.support == PIN_SUPPORTED) {
      um_gpio_ioctl(UM_GPIO_SET_OUT1, state);
    }
  }
}

// **************************************************************************
// get state of output OUT0
int um_gpio_get_out0(void)
{
  if (pin_support.out0.support == PIN_SUPPORTED) {
    int result = um_gpio_ioctl(UM_GPIO_GET_OUT0, 0);
    return result >= 0 ? result : 0;
  } else if (pin_support.out0.support == PIN_UNSUPPORTED) {
    return 0;
  } else {
    int result = um_gpio_init_pin_and_get_binary_value(&pin_support.out0);
    return result >= 0 ? result : 0;
  }
}

// **************************************************************************
// get state of output OUT1
int um_gpio_get_out1(void)
{
  if (pin_support.out1.support == PIN_SUPPORTED) {
    int result = um_gpio_ioctl(UM_GPIO_GET_OUT1, 0);
    return result >= 0 ? result : 0;
  } else if (pin_support.out1.support == PIN_UNSUPPORTED) {
    return 0;
  } else {
    int result = um_gpio_init_pin_and_get_binary_value(&pin_support.out1);
    return result >= 0 ? result : 0;
  }
}

// **************************************************************************
// get state of input BIN0
int um_gpio_get_bin0(void)
{
  if (pin_support.bin0.support == PIN_SUPPORTED) {
    int result = um_gpio_ioctl(UM_GPIO_GET_BIN0, 0);
    return result >= 0 ? result : 0;
  } else if (pin_support.bin0.support == PIN_UNSUPPORTED) {
    return 0;
  } else {
    int result = um_gpio_init_pin_and_get_binary_value(&pin_support.bin0);
    return result >= 0 ? result : 0;
  }
}

// **************************************************************************
// get state of input BIN1
int um_gpio_get_bin1(void)
{
  if (pin_support.bin1.support == PIN_SUPPORTED) {
    int result = um_gpio_ioctl(UM_GPIO_GET_BIN1, 0);
    return result >= 0 ? result : 0;
  } else if (pin_support.bin1.support == PIN_UNSUPPORTED) {
    return 0;
  } else {
    int result = um_gpio_init_pin_and_get_binary_value(&pin_support.bin1);
    return result >= 0 ? result : 0;
  }
}

// **************************************************************************
// get state of input BIN2
int um_gpio_get_bin2(void)
{
  if (pin_support.bin2.support == PIN_SUPPORTED) {
    int result = um_gpio_ioctl(UM_GPIO_GET_BIN2, 0);
    return result >= 0 ? result : 0;
  } else if (pin_support.bin2.support == PIN_UNSUPPORTED) {
    return 0;
  } else {
    int result = um_gpio_init_pin_and_get_binary_value(&pin_support.bin2);
    return result >= 0 ? result : 0;
  }
}

// **************************************************************************
// get state of input BIN3
int um_gpio_get_bin3(void)
{
  if (pin_support.bin3.support == PIN_SUPPORTED) {
    int result = um_gpio_ioctl(UM_GPIO_GET_BIN3, 0);
    return result >= 0 ? result : 0;
  } else if (pin_support.bin3.support == PIN_UNSUPPORTED) {
    return 0;
  } else {
    int result = um_gpio_init_pin_and_get_binary_value(&pin_support.bin3);
    return result >= 0 ? result : 0;
  }
}

// **************************************************************************
// get availability of output OUT0
int um_gpio_has_out0(void)
{
  if (pin_support.out0.support != PIN_SUPPORT_UNKNOWN) {
    return pin_support.out0.support == PIN_SUPPORTED;
  } else {
    um_gpio_init_pin_and_get_binary_value(&pin_support.out0);
    return pin_support.out0.support == PIN_SUPPORTED;
  }
}

// **************************************************************************
// get availability of output OUT1
int um_gpio_has_out1(void)
{
  if (pin_support.out1.support != PIN_SUPPORT_UNKNOWN) {
    return pin_support.out1.support == PIN_SUPPORTED;
  } else {
    um_gpio_init_pin_and_get_binary_value(&pin_support.out1);
    return pin_support.out1.support == PIN_SUPPORTED;
  }
}

// **************************************************************************
// get availability of input BIN0
int um_gpio_has_bin0(void)
{
  if (pin_support.bin0.support != PIN_SUPPORT_UNKNOWN) {
    return pin_support.bin0.support == PIN_SUPPORTED;
  } else {
    um_gpio_init_pin_and_get_binary_value(&pin_support.bin0);
    return pin_support.bin0.support == PIN_SUPPORTED;
  }
}

// **************************************************************************
// get availability of input BIN1
int um_gpio_has_bin1(void)
{
  if (pin_support.bin1.support != PIN_SUPPORT_UNKNOWN) {
    return pin_support.bin1.support == PIN_SUPPORTED;
  } else {
    um_gpio_init_pin_and_get_binary_value(&pin_support.bin1);
    return pin_support.bin1.support == PIN_SUPPORTED;
  }
}

// **************************************************************************
// get availability of input BIN2
int um_gpio_has_bin2(void)
{
  if (pin_support.bin2.support != PIN_SUPPORT_UNKNOWN) {
    return pin_support.bin2.support == PIN_SUPPORTED;
  } else {
    um_gpio_init_pin_and_get_binary_value(&pin_support.bin2);
    return pin_support.bin2.support == PIN_SUPPORTED;
  }
}

// **************************************************************************
// get availability of input BIN3
int um_gpio_has_bin3(void)
{
  if (pin_support.bin3.support != PIN_SUPPORT_UNKNOWN) {
    return pin_support.bin3.support == PIN_SUPPORTED;
  } else {
    um_gpio_init_pin_and_get_binary_value(&pin_support.bin3);
    return pin_support.bin3.support == PIN_SUPPORTED;
  }
}

// **************************************************************************
// get internal temperature
int um_gpio_get_temperature(void)
{
  return um_gpio_ioctl(UM_GPIO_GET_TEMPERATURE, 0);
}

// **************************************************************************
// get supply voltage
int um_gpio_get_voltage(void)
{
  return um_gpio_ioctl(UM_GPIO_GET_VOLTAGE, 0);
}

