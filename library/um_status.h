// **************************************************************************
//
// Functions for reading out status
//
// Copyright (C) 2017-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_STATUS_H_
#define _UM_STATUS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define ADVANTECH_OID_SUBTREE      "1.3.6.1.4.1.30140"
#define ADVANTECH_DEVICES_OID_FMT  ADVANTECH_OID_SUBTREE ".1.%s"
#define GENERIC_PRODUCT_OID        ADVANTECH_OID_SUBTREE ".1.1000"

// execute the program "status" and return its output
extern const char *um_status_exec(const char *category, int max_age);

// find value of one parameter in the status information
extern char *um_status_find(const char *status, const char *param);

// get the value of the given status parameter
extern char *um_status_get(const char *category, const char *param);

// get the product name
extern const char *um_status_get_product_name(void);

// get /etc/os-release information
extern const char *um_status_get_os_info(const char *field);

// check /etc/os-release space-separated value
extern int um_status_has_os_info_token(const char *field_name, const char *field_value);

// get the platform OID
extern const char *um_status_get_product_oid(void);

// get the platform name
extern const char *um_status_get_platform_name(void);

// check whether the product is certified for a given country, e.g. "US"
extern int um_status_check_country(const char *name);

// get the firmware version
extern const char *um_status_get_firmware_version(void);

// get the serial number
extern const char *um_status_get_serial_number(void);

// get the hardware UUID
extern const char *um_status_get_hardware_uuid(void);

// get the model of the given module
extern const char *um_status_get_module_model(int module);

// get the revision of the given module
extern const char *um_status_get_module_revision(int module);

// get the IMEI of the given module
extern const char *um_status_get_module_imei(int module);

// get the ICCID of the given module
extern const char *um_status_get_module_iccid(int module);

//  get the ESN of the given module
extern const char *um_status_get_module_esn(int module);

// get the MEID of the given module
extern const char *um_status_get_module_meid(int module);

// get the interface of the given module
extern const char *um_status_get_module_iface(int module);

// get the registration state of the given module
extern const char *um_status_get_mobile_registration(int module);

// get the operator of the given module
extern const char *um_status_get_mobile_operator(int module);

// get the technology of the given module
extern const char *um_status_get_mobile_technology(int module);

// get the PLMN of the given module
extern unsigned int um_status_get_mobile_plmn(int module);

// get the cell of the given module
extern unsigned int um_status_get_mobile_cell(int module);

// get the LAC of the given module
extern unsigned int um_status_get_mobile_lac(int module);

// get the channel of the given module
extern unsigned int um_status_get_mobile_channel(int module);

// get the signal strength of the given module
extern int um_status_get_mobile_strength(int module);

// get the signal quality of the given module
extern int um_status_get_mobile_quality(int module);

#ifdef __cplusplus
}
#endif

#endif

