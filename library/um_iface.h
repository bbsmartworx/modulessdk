// **************************************************************************
//
// Functions for getting information on network interfaces
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#ifndef _UM_IFACE_H_
#define _UM_IFACE_H_

#include <arpa/inet.h>      // in6_addr

#ifdef __cplusplus
extern "C" {
#endif

// maximal number of IPv6 addresses
#define IFACE_IPV6_MAX          16

// network interface properties
typedef struct {
  int                   index;
  int                   flags;
  int                   up;
  int                   run;
  unsigned char         hwaddr[6];
  unsigned int          ipaddr;
  unsigned int          netmask;
  struct in6_addr       ipaddr6[IFACE_IPV6_MAX];
  unsigned int          netmask6[IFACE_IPV6_MAX];
  unsigned int          scope6[IFACE_IPV6_MAX];
  unsigned int          mtu;
} iface_info_t;

// correct interface MAC address when VRRP is enabled
void um_iface_fix_hwaddr(const char *name, unsigned char *hwaddr);

// get interface properties
void um_iface_get_info(const char *name, iface_info_t *info_ptr);

// get interface hardware address
uint64_t um_iface_get_hwaddr(const char *name);

// get interface IP address
unsigned int um_iface_get_ip(const char *name);

// get name of the interface to be used to send data to a destination address
const char *um_iface_get_name(unsigned int ip);

#ifdef __cplusplus
}
#endif

#endif // _UM_IFACE_H_