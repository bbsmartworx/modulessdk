// **************************************************************************
//
// Functions for getting parameters of CGI script
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#ifndef _UM_CGI_H_
#define _UM_CGI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <netinet/in.h>

// start CGI script parameters processing
extern void um_cgi_begin(void);

// finish CGI script parameters processing
extern void um_cgi_end(void);

// check if query is ok (without CSRF token check)
extern int um_cgi_query_ok_not_csrf(void);

// check if query is ok (with CSRF token check)
extern int um_cgi_query_ok(void);

// get CGI script parameter (raw string)
extern int um_cgi_get_raw(const char *name, char **value_ptr);

// get CGI script parameter (string)
extern int um_cgi_get_str(const char *name, char **value_ptr, int no_spaces);

// get CGI script parameter (multi-lite text)
extern int um_cgi_get_text(const char *name, char **value_ptr);

// get CGI script parameter (integer)
extern int um_cgi_get_int(const char *name, unsigned int *value_ptr);

// get CGI script parameter (boolean)
extern int um_cgi_get_bool(const char *name, unsigned int *value_ptr);

// get CGI script parameter (IP address)
extern int um_cgi_get_ip(const char *name, unsigned int *value_ptr, int zero);

// get CGI script parameter (IPv6 address)
extern int um_cgi_get_ip6(const char *name, struct in6_addr **value_ptr, int zero);

// get CGI script parameter (MAC address)
extern int um_cgi_get_mac(const char *name, char **value_ptr, int zero);

// get name of logged in user
extern const char *um_cgi_get_user_name(void);

// check role of logged in user
extern int um_cgi_is_admin_role(void);

// get user ID of logged in user
extern uid_t um_cgi_get_user_uid(void);

// get group ID of logged in user
extern gid_t um_cgi_get_user_gid(void);

// get nonce value for JS when unsafe-inline is off
// Caller should use the value immediately or duplicate it as it can be overwritten by a next getenv() call.
extern const char *um_cgi_get_nonce_js(void);

// get nonce value for CSS when unsafe-inline is off
// Caller should use the value immediately or duplicate it as it can be overwritten by a next getenv() call.
extern const char *um_cgi_get_nonce_css(void);

#ifdef __cplusplus
}
#endif

#endif

