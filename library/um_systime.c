// **************************************************************************
//
// Function for getting monotonic system time
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <time.h>
#include <sys/time.h>

#include "um_systime.h"

// **************************************************************************
// get monotonic system time
time_t um_systime(void)
{
#ifdef CLOCK_MONOTONIC
  struct timespec       ts;

  // get monotonic time
  if (clock_gettime(CLOCK_MONOTONIC, &ts) == 0) {
    return ts.tv_sec;
  }
#else
  struct timeval        tv;

  // get real time if monotonic time isn't available
  if (gettimeofday(&tv, NULL) == 0) {
    return tv.tv_sec;
  }
#endif

  // operation failed
  return -1;
}

