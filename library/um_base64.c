// **************************************************************************
//
// Functions for BASE64 encoding/decoding
//
// Copyright (C) 2015-2024 Advantech Czech s.r.o.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//
// **************************************************************************

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>

#include "um_base64.h"

// **************************************************************************

// table for BASE64 encoding
static const char UM_BASE64_ENCODE[64] = {
  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
  'Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f',
  'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
  'w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/'
};

// table for BASE64 decoding
static const unsigned char UM_BASE64_DECODE[256] = {
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,62,64,62,64,63,
  52,53,54,55,56,57,58,59,60,61,64,64,64,64,64,64,
  64, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,
  15,16,17,18,19,20,21,22,23,24,25,64,64,64,64,64,
  64,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
  41,42,43,44,45,46,47,48,49,50,51,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,
  64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64
};

// **************************************************************************
// encode BASE64
void um_base64_encode(const unsigned char *src, char *dst, size_t size)
{
  unsigned char         b0, b1, b2;
  int                   padding;

  padding = 0;

  while (size > 0) {
    b0 = (unsigned char)(size ? (size--, *src++) : (padding++, 0));
    b1 = (unsigned char)(size ? (size--, *src++) : (padding++, 0));
    b2 = (unsigned char)(size ? (size--, *src++) : (padding++, 0));
    *dst++ = UM_BASE64_ENCODE[b0 >> 2];
    *dst++ = UM_BASE64_ENCODE[((b0 & 0x03) << 4) | (b1 >> 4)];
    *dst++ = UM_BASE64_ENCODE[((b1 & 0x0F) << 2) | (b2 >> 6)];
    *dst++ = UM_BASE64_ENCODE[b2 & 0x3F];
  }

  *dst = '\0';

  while (padding-- > 0) {
    *--dst = '=';
  }
}

// **************************************************************************
// decode BASE64
size_t um_base64_decode(const char *src, unsigned char *dst)
{
  unsigned char         act, prev;
  size_t                size;
  int                   phase;

  phase = 0;
  prev  = 0;
  size  = 0;

  while (*src) {
    act = UM_BASE64_DECODE[(unsigned char)*src++];
    if (act < 64) {
      switch (phase & 0x03) {
        case 0x01:
          *dst++ = (unsigned char)((prev << 2 ) | ((act & 0x30) >> 4));
          size++;
          break;
        case 0x02:
          *dst++ = (unsigned char)(((prev & 0x0F) << 4) | ((act & 0x3C) >> 2));
          size++;
          break;
        case 0x03:
          *dst++ = (unsigned char)(((prev & 0x03) << 6) | act);
          size++;
          break;
      }
      prev = act;
      phase++;
    }
  }

  return size;
}

// **************************************************************************
// encode BASE64 (result shall be freed by the caller)
char *um_base64_encode_str(const char *str)
{
  char                  *buf;
  size_t                size;

  if (!str) {
    return NULL;
  }

  size = strlen(str);
  buf  = malloc(4 * ((size + 2) / 3) + 1);
  if (buf) {
    um_base64_encode((const unsigned char *)str, buf, size);
  }

  return buf;
}

// **************************************************************************
// decode BASE64 (result shall be freed by the caller)
char *um_base64_decode_str(const char *str)
{
  char                  *buf;
  size_t                size;

  if (!str) {
    return NULL;
  }

  buf = malloc(strlen(str) + 1);
  if (buf) {
    size = um_base64_decode(str, (unsigned char *)buf);
    buf[size] = '\0';
  }

  return buf;
}

