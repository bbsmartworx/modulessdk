PLATFORM  ?= v1 v2i v3 v4 v4i
PLATFORMS ?= $(PLATFORM)

DIRS := library modules

all:
	@for DIR in $(DIRS); do \
		for PLATFORM in $(PLATFORMS); do \
			$(MAKE) -C $$DIR PLATFORM=$$PLATFORM || exit; \
		done; \
	done

clean:
	@for DIR in $(DIRS); \
		do $(MAKE) -C $$DIR clean; \
	done
	@rm -rf images
